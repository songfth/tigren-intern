<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Override\Magento\Customer\Model\Data;

/**
 * Class Address
 * @package Tigren\AdvancedCheckout\Override\Magento\Customer\Model\Data
 */
class Address extends \Magento\Customer\Model\Data\Address
{
    /**
     *
     */
    const LOCATION_TYPE = 'location_type';
    /**
     *
     */
    const NEAREST_LANDMARK = 'nearest_landmark';


    /**
     * @return mixed|null
     */
    public function getLocationType()
    {
        return $this->_get(self::LOCATION_TYPE);
    }

    /**
     * @param $locationType
     * @return $this
     */
    public function setLocationType($locationType)
    {
        return $this->setData(self::LOCATION_TYPE, $locationType);
    }

    /**
     * @return mixed|null
     */
    public function getNearestLandmark()
    {
        return $this->_get(self::NEAREST_LANDMARK);
    }

    /**
     * @param $nearestLandmark
     * @return $this
     */
    public function setNearestLandmark($nearestLandmark)
    {
        return $this->setData(self::NEAREST_LANDMARK, $nearestLandmark);
    }

}
