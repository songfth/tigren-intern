/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/**
 * Checkout adapter for customer data storage
 */
define([
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Checkout/js/model/address-converter',
    'underscore'
], function (
    addressList,
    quote,
    checkoutData,
    createShippingAddress,
    selectShippingAddress,
    selectShippingMethodAction,
    paymentService,
    selectPaymentMethodAction,
    addressConverter,
    _
) {
    'use strict';

    var mixin = {
        /**
         * reload resolved estimated address to quote
         *
         */
        reloadShippingAddress: function () {
            var addressData,
                isShippingAddressInitialized;
            isShippingAddressInitialized = addressList.some(function (addrs) {
                if (addrs.isDefaultShipping()) {
                    addressData = addrs;
                    selectShippingAddress(addressData);

                    return true;
                }

                return false;
            });
            if (!isShippingAddressInitialized && addressList().length === 1) {
                addressData =  addressList()[0];
                selectShippingAddress(addressData);
            }

        }
    };

    return function (target) {
        return _.extend(target, mixin);
    };
});