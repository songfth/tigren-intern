/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'uiComponent',
    'Magento_Ui/js/modal/confirm',
    'mage/url',
    'jquery',
    'Magento_Checkout/js/model/cart/totals-processor/default',
    'Magento_Checkout/js/model/cart/cache',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/step-navigator'
], function (Component,
             confirm,
             urlBuilder,
             $,
             totalsDefaultProvider,
             cartCache,
             quote,
             defaultProcessor,
             customerAddressProcessor,
             resourceUrlManager,
             storage,
             shippingService,
             rateRegistry,
             errorProcessor,
             selectShippingMethodAction,
             checkoutData,
             stepNavigator
    ) {
    'use strict';

    var processors = [];

    processors.default =  defaultProcessor;
    processors['customer-address'] = customerAddressProcessor;

    return function (Component) {
        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/summary/item/details'
            },

            /**
             * @param {Object} quoteItem
             * @return {String}
             */
            getValue: function (quoteItem) {
                return quoteItem.name;
            },

            removeItem: function () {
                var self = this;
                event.stopPropagation();
                var elm = event.currentTarget;
                confirm({
                    content: $.mage.__('Are you sure you would like to remove this item from the shopping cart?'),
                    actions: {
                        /** @inheritdoc */
                        confirm: function () {
                            self.deleteItem($(elm).data('cart-item'));
                        },

                        /** @inheritdoc */
                        always: function (e) {
                            e.stopImmediatePropagation();
                        }
                    }
                });
            },


            deleteItem: function (itemId) {
                var self = this;
                var url = urlBuilder.build('advancedcheckout/summary/removeItem');

                $.ajax({
                    url: url,
                    data: {item_id: itemId, form_key: $.mage.cookies.get('form_key')},
                    type: 'post',
                    dataType: 'json',

                    /** @inheritdoc */
                    beforeSend: function () {
                    },

                    /** @inheritdoc */
                    complete: function () {
                    }
                })
                    .done(function (response) {
                        var msg;

                        if(response.redirect_url){
                            window.location.href = response.redirect_url;
                            return false;
                        }

                        if (response.success) {
                            if (response.reload) {
                                location.reload();
                            }
                            selectShippingMethodAction(null);
                            checkoutData.setSelectedShippingRate(null);
                            cartCache.set('totals', null);
                            totalsDefaultProvider.estimateTotals();
                            self.getRates(quote.shippingAddress());
                            var currentStep = stepNavigator.steps()[stepNavigator.getActiveItemIndex()];
                            if (currentStep.code == 'payment'){
                                stepNavigator.navigateTo('shipping');
                            }
                        } else {
                            msg = response['error_message'];

                            if (msg) {
                                alert({
                                    content: msg
                                });
                            }
                        }
                    })
                    .fail(function (error) {
                        console.log(JSON.stringify(error));
                    });


            },

            getRates: function (address) {
                var type = address.getType();
                rateRegistry.reset();
                if (processors[type]) {
                    processors[type].getRates(address);
                } else {
                    processors.default.getRates(address);
                }
                return {
                    /**
                     * @param {String} type
                     * @param {*} processor
                     */
                    registerProcessor: function (type, processor) {
                        processors[type] = processor;
                    }
                };
            }

        });
    }
});
