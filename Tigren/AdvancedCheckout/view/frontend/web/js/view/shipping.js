/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Tigren_AdvancedCheckout/js/action/edit-shipping-address',
    'Magento_Customer/js/customer-data',
    'mage/utils/objects',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'mage/url',
    'Magento_Customer/js/model/customer/address',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address'
], function (
    $,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t,
    editShippingAddress,
    customerData,
    mageUtils,
    resourceUrlManager,
    storage,
    errorProcessor,
    url,
    address,
    defaultProcessor,
    customerAddressProcessor
) {
    'use strict';
    var processors = [];

    processors.default =  defaultProcessor;
    processors['customer-address'] = customerAddressProcessor;

    var popUp = null,
        countryData = customerData.get('directory-data');
    return function (Component) {
        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/shipping',
                shippingFormTemplate: 'Magento_Checkout/shipping-address/form',
                shippingMethodListTemplate: 'Magento_Checkout/shipping-address/shipping-method-list',
                shippingMethodItemTemplate: 'Magento_Checkout/shipping-address/shipping-method-item'
            },
            visible: ko.observable(!quote.isVirtual()),
            errorValidationMessage: ko.observable(false),
            isCustomerLoggedIn: customer.isLoggedIn,
            isFormPopUpVisible: formPopUpState.isVisible,
            isFormInline: addressList().length === 0,
            isNewAddressAdded: ko.observable(false),
            saveInAddressBook: 1,
            quoteIsVirtual: quote.isVirtual(),
            isDefault: ko.observable(false),

            hasUpdatedAddress: formPopUpState.hasUpdatedAddress,

            /**
             * @return {exports}
             */
            initialize: function () {
                var self = this;

                this._super();

                this.hasUpdatedAddress.subscribe(function (address) {
                    if (address) {
                        self.getPopUp().openModal();
                    }
                });
                return this;
            },

            /**
             * Navigator change hash handler.
             *
             * @param {Object} step - navigation step
             */
            navigate: function (step) {
                step && step.isVisible(true);
            },

            /**
             * Show address form popup
             */
            showFormPopUp: function () {
                this.hasUpdatedAddress(false);
                this.isFormPopUpVisible(true);
            },

            /**
             * @return {*}
             */
            getPopUp: function () {
                var self = this,
                    buttons;

                if (!popUp) {
                    buttons = this.popUpForm.options.buttons;
                    this.popUpForm.options.buttons = [
                        {
                            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                            click: self.saveNewAddress.bind(self)
                        }
                    ];

                    /** @inheritdoc */
                    this.popUpForm.options.closed = function () {
                        self.isFormPopUpVisible(false);
                    };

                    this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                    this.popUpForm.options.keyEventHandlers = {
                        escapeKey: this.onClosePopUp.bind(this)
                    };

                    /** @inheritdoc */
                    this.popUpForm.options.opened = function () {
                        // Store temporary address for revert action in case when user click cancel action
                        var addressData,
                            isNewAddressPopup = false;

                        if (self.hasUpdatedAddress()) {
                            addressData = addressConverter.quoteAddressToFormAddressData(
                                self.hasUpdatedAddress()
                            );
                            if(self.hasUpdatedAddress().isDefaultShipping()){
                                self.isDefault(true);
                            }else{
                                self.isDefault(false);
                            }
                            checkoutData.setShippingAddressFromData($.extend(true, {}, addressData));
                        } else {
                            isNewAddressPopup = true;
                        }

                        self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
                        if (isNewAddressPopup) {
                            self.reset();
                        } else {
                            self.reset();
                            self.temporaryAddress.customer_address_id = addressData.customer_address_id;
                            _.each(self.temporaryAddress, function (value, name) {
                                if (name !== 'custom_attributes') {
                                    this.source.set('shippingAddress.' + name, value);
                                }
                                setTimeout(function () {
                                    self.source.set('shippingAddress.custom_attributes.location_type', self.temporaryAddress.custom_attributes.location_type);
                                }, 100);
                                setTimeout(function () {
                                    self.source.set('shippingAddress.custom_attributes.nearest_landmark', self.temporaryAddress.custom_attributes.nearest_landmark);
                                }, 200);
                            }, self);
                        }
                    };
                }

                if (popUp) {
                    popUp.closeModal();
                }
                popUp = modal(this.popUpForm.options, $(this.popUpForm.element));

                return popUp;
            },

            /**
             * Convert object to array
             * @param {Object} object
             * @returns {Array}
             */
            objectToArray: function (object) {
                var convertedArray = [];

                $.each(object, function (key) {
                    return typeof object[key] === 'string' ? convertedArray.push(object[key]) : false;
                });

                return convertedArray.slice(0);
            },

            /**
             * Save new shipping address
             */
            saveNewAddress: function () {
                var self = this,
                    addressFormData,
                    customerAddressId,
                    newShippingAddress,
                    updatedShippingAddress;

                this.source.set('params.invalid', false);
                this.triggerShippingDataValidateEvent();

                if (!this.source.get('params.invalid')) {
                    addressFormData = this.source.get('shippingAddress');
                    customerAddressId = addressFormData.customer_address_id;
                    if (customerAddressId && self.hasUpdatedAddress()) {
                        $.ajax({
                            url: url.build('advancedcheckout/customer/updateAddress'),
                            method: 'post',
                            data: addressFormData,
                            datatype: 'json',
                            beforeSend: function () {
                                $('body').trigger('processStart');
                            },
                            complete: function () {
                                $('body').trigger('processStop');
                            },
                            success: function (response) {
                                if (response.success) {
                                    shippingService.isLoading(true);
                                    var addressData = $.extend(true, {}, addressFormData),
                                        region,
                                        regionName = addressData.region;

                                    if (mageUtils.isObject(addressData.street)) {
                                        addressData.street = self.objectToArray(addressData.street);
                                    }

                                    addressData.region = {
                                        'region_id': addressData['region_id'],
                                        'region_code': addressData['region_code'],
                                        region: regionName
                                    };

                                    addressData.id = addressData.customer_address_id;

                                    addressData['default_shipping'] = self.isDefault();

                                    if (addressData['region_id'] &&
                                        countryData()[addressData['country_id']] &&
                                        countryData()[addressData['country_id']].regions
                                    ) {
                                        region = countryData()[addressData['country_id']].regions[addressData['region_id']];

                                        if (region) {
                                            addressData.region['region_id'] = addressData['region_id'];
                                            addressData.region['region_code'] = region.code;
                                            addressData.region.region = region.name;
                                        }
                                    } else if (
                                        !addressData['region_id'] &&
                                        countryData()[addressData['country_id']] &&
                                        countryData()[addressData['country_id']].regions
                                    ) {
                                        addressData.region['region_code'] = '';
                                        addressData.region.region = '';
                                    }
                                    delete addressData['region_id'];

                                    updatedShippingAddress = editShippingAddress(address(addressData));
                                    selectShippingAddress(updatedShippingAddress);
                                    checkoutData.setSelectedShippingAddress(updatedShippingAddress.getKey());
                                    self.getPopUp().closeModal();
                                    self.hasUpdatedAddress(false);
                                    self.getRates(updatedShippingAddress);

                                }
                            }
                        });
                    } else {
                        // if user clicked the checkbox, its value is true or false. Need to convert.
                        addressFormData['save_in_address_book'] = self.saveInAddressBook ? 1 : 0;

                        // New address must be selected as a shipping address
                        newShippingAddress = createShippingAddress(addressFormData);
                        selectShippingAddress(newShippingAddress);
                        checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                        checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressFormData));
                        self.getPopUp().closeModal();
                        self.isNewAddressAdded(true);
                    }
                }
            },

            getRates: function (address) {
                var type = address.getType();
                rateRegistry.reset();
                if (processors[type]) {
                    processors[type].getRates(address);
                } else {
                    processors.default.getRates(address);
                }
                return {
                    /**
                     * @param {String} type
                     * @param {*} processor
                     */
                    registerProcessor: function (type, processor) {
                        processors[type] = processor;
                    }
                };
            }
        });
    }
});
