/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'ko',
    'Magento_Ui/js/form/element/abstract',
    'jquery',
    'Magento_Ui/js/lib/validation/validator'
], function (ko, Abstract, $, validator) {
    'use strict';

    return Abstract.extend({
        defaults: {
            visible: true,
            value: ko.observable()
        },

        /**
         * @returns {*|void|Element}
         */
        initObservable: function () {
            var self = this;
            var regex = /^[0-9]+$/;
            this._super();
            self.value.subscribe(function () {
                if ((self.value() == "") || (self.value().substr(0, 4) != "+967") || (!self.value().substr(5).match(regex))) {
                    self.value("+967");
                }
            });
            return this;
        },
        validate: function () {
            var value = this.value(),
                result = validator(this.validation, value, this.validationParams),
                message = !this.disabled() && this.visible() ? result.message : '',
                isValid = this.disabled() || !this.visible() || result.passed;
            if (this.value() == "+967") {
                message = "This is required field.";
                isValid = false;
            }
            this.error(message);
            this.bubble('error', message);

            //TODO: Implement proper result propagation for form
            if (!isValid) {
                this.source.set('params.invalid', true);
            }

            return {
                valid: isValid,
                target: this
            };
        },

    });
});
