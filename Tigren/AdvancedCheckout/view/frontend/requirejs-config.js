/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/template/authentication.html':
                'Tigren_AdvancedCheckout/template/authentication.html',
            'Magento_Checkout/js/model/shipping-address/form-popup-state':
                'Tigren_AdvancedCheckout/js/model/shipping-address/form-popup-state',
            'Magento_Checkout/js/view/shipping-address/list':
                'Tigren_AdvancedCheckout/js/view/shipping-address/list',
            'Magento_Checkout/js/proceed-to-checkout':
                'Tigren_AdvancedCheckout/js/proceed-to-checkout',
            'Magento_Checkout/js/view/shipping-address/address-renderer/default':
                'Tigren_AdvancedCheckout/js/view/shipping-address/address-renderer/default',
            'Magento_Customer/js/model/customer/address':
                'Tigren_AdvancedCheckout/js/model/customer/address',
            'Magento_Checkout/js/model/new-customer-address':
                'Tigren_AdvancedCheckout/js/model/new-customer-address',
            'Magento_SalesRule/js/view/summary/discount':
                'Tigren_AdvancedCheckout/js/view/summary/discount',
            'Magento_Checkout/js/model/shipping-rate-registry':
                'Tigren_AdvancedCheckout/js/model/shipping-rate-registry'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/summary/item/details': {
                'Tigren_AdvancedCheckout/js/view/summary/item/details': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Tigren_AdvancedCheckout/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Tigren_AdvancedCheckout/js/view/shipping': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Tigren_AdvancedCheckout/js/model/checkout-data-resolver': true
            }
        }
    }
};