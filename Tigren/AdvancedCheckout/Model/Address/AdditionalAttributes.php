<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Model\Address;

use Magento\Framework\Api\AbstractSimpleObject;


/**
 * Class AdditionalAttributes
 * @package Tigren\AdvancedCheckout\Model\Address
 */
class AdditionalAttributes extends AbstractSimpleObject implements \Magento\Customer\Api\Data\AddressExtensionInterface
{
    /**
     * @param $locationType
     */
    public function setLocationType($locationType)
    {
        $this->setData('location_type', $locationType);
    }

    /**
     * @return mixed|null
     */
    public function getLocationType()
    {
        return $this->_get('location_type');
    }

    /**
     * @param $nearestLandmark
     */
    public function setNearestLandmark($nearestLandmark)
    {
        $this->setData('nearest_landmark', $nearestLandmark);
    }

    /**
     * @return mixed|null
     */
    public function getNearestLandmark()
    {
        return $this->_get('nearest_landmark');
    }

}