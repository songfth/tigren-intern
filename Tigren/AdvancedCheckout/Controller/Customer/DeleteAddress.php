<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

/**
 * Delete customer address
 */
class DeleteAddress extends Action
{
    protected $_addressRepository;

    /**
     * DeleteAddress constructor.
     * @param Context $context
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository

    ) {
        $this->_addressRepository = $addressRepository;
        parent::__construct($context);
    }

    /**
     * Execute delete address
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $result = [
            'success' => false,
            'message' => ''
        ];

        $addressId = $this->getRequest()->getParam('id');

        try {
            $this->_addressRepository->deleteById($addressId);
            $result['success'] = true;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $result['message'] = $e->getMessage();
        }

        return $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($result)
        );
    }
}