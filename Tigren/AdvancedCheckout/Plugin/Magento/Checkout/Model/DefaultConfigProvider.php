<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Checkout\Model;
use Magento\Framework\UrlInterface;

class DefaultConfigProvider
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;

    public function __construct(
        \Magento\Customer\Model\AddressFactory $addressFactory,
        UrlInterface $urlBuilder
    ) {
        $this->addressFactory = $addressFactory;
        $this->urlBuilder = $urlBuilder;
    }


    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, $result)
    {
        if (!empty($result['customerData']['addresses'])) {
            foreach ($result['customerData']['addresses'] as &$address) {
                $customAttributes = [];
                $addressModel = $this->addressFactory->create()->load($address['id']);
                $fields = ['location_type', 'nearest_landmark'];
                foreach ($fields as $field) {
                    if ($addressModel->getData($field)) {
                        $customAttributes[$field] = $addressModel->getData($field);
                    }
                }
                $address['custom_attributes'] = $customAttributes;
            }
        }
        $result['registerUrl'] = $this->urlBuilder->getUrl('customer/account/create',['is_checkout'=>true]);
        return $result;
    }
}