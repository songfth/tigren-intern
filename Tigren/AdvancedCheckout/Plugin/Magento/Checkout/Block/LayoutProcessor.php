<?php

/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Checkout\Block;

use Closure;
use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\Quote;
use Magento\Store\Api\StoreResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\AttributeMapper;

/**
 * Class LayoutProcessor
 * @package Marginframe\Thaiaddress\Plugin\Magento\Checkout\Block
 */
class LayoutProcessor
{
    /**
     * @var CheckoutSession
     */
    public $checkoutSession;

    /**
     * @var null
     */
    public $quote = null;

    /**
     * @var null
     */
    public $customer = null;

    /**
     * @var AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var AttributeMerger
     */
    protected $merger;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DirectoryHelper
     */
    private $directoryHelper;

    /**
     * LayoutProcessor constructor.
     *
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     * @param CheckoutSession $checkoutSession
     * @param UrlInterface $urlBuilder
     * @param StoreResolverInterface $storeResolver @deprecated
     * @param DirectoryHelper $directoryHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        CheckoutSession $checkoutSession,
        UrlInterface $urlBuilder,
        StoreResolverInterface $storeResolver,
        DirectoryHelper $directoryHelper,
        StoreManagerInterface $storeManager = null
    ) {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->checkoutSession = $checkoutSession;
        $this->urlBuilder = $urlBuilder;
        $this->directoryHelper = $directoryHelper;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param Closure $proceed
     * @param array $jsLayout
     * @return mixed
     * @throws LocalizedException
     */
    public function aroundProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        Closure $proceed,
        array $jsLayout
    ) {
        $jsLayoutResult = $proceed($jsLayout);
        if ($this->getQuote()->isVirtual()) {
            return $jsLayoutResult;
        }

        if (isset($jsLayoutResult['components']['checkout']['component'])) {
            $jsLayoutResult['components']['checkout']['component'] = 'Tigren_AdvancedCheckout/js/view/onepage';
            $jsLayoutResult['components']['checkout']['config']['template'] = 'Tigren_AdvancedCheckout/onepage';
        }
        $locationTypeShipping = [
            'component' => 'Tigren_AdvancedCheckout/js/form/element/location-type',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'Tigren_AdvancedCheckout/form/element/location-type',
                'additionalClasses' => 'location-type'
            ],
            'label' => __('Location Type'),
            'dataScope' => 'shippingAddress.custom_attributes.location_type',
            'provider' => 'checkoutProvider',
            'sortOrder' => 100
        ];
        $nearestLandmarkShipping = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'additionalClasses' => 'nearest-landmark'
            ],
            'label' => __('Nearest Landmark'),
            'dataScope' => 'shippingAddress.custom_attributes.nearest_landmark',
            'provider' => 'checkoutProvider',
            'sortOrder' => 101
        ];
        $telephone = [
            'component' => 'Tigren_AdvancedCheckout/js/form/element/telephone',
            'config' => [
                'customScope' => 'shippingAddress',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'additionalClasses' => 'telephone',
            ],
            'validation'=>[
                'required-entry'=>true
            ],
            'label' => __('Phone Number'),
            'dataScope' => 'shippingAddress.telephone',
            'provider' => 'checkoutProvider',
            'sortOrder' => 100
        ];
        if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['location_type'] = $locationTypeShipping;
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['nearest_landmark'] = $nearestLandmarkShipping;
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['telephone'] = $telephone;
        }
        if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'])) {
            foreach ($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'] as $key => $payment) {
                if ($key === 'before-place-order') {
                    continue;
                }
                unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['location_type']);
                unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['nearest_landmark']);
                //$jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['location_type'] = $cityBilling;
                //$jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['city_id'] = $cityIdBiling;
                $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['telephone']=$telephone;
                $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['country_id']['visible']=false;
            }
        }
        return $jsLayoutResult;
    }

    /**
     * Get quote
     *
     * @return Quote|null
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }
}
