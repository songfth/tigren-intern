<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Customer\Controller\Account;

class Create
{
    protected $coreSession;

    protected $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    )
    {
        $this->request = $request;
        $this->coreSession = $coreSession;
    }


    public function afterExecute(\Magento\Customer\Controller\Account\Create $subject, $object)
    {
        if ($this->request->getParam('is_checkout')){
            $this->coreSession->setIsCheckout(true);
        }else{
            $this->coreSession->setIsCheckout(false);
        }

        return $object;
    }
}
