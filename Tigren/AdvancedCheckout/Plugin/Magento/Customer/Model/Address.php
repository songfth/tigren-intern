<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\AdvancedCheckout\Plugin\Magento\Customer\Model;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Directory\Helper\Data as HelperData;

class Address
{
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var HelperData
     */
    protected $helperData;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    public function __construct(
        \Magento\Framework\Reflection\DataObjectProcessor $dataProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->request = $request;
    }

    /**
     * @param \Magento\Customer\Model\Address $subject
     * @param callable $proceed
     * @param AddressInterface $address
     * @return mixed
     */
    public function aroundUpdateData(
        \Magento\Customer\Model\Address $subject,
        callable $proceed,
        AddressInterface $address
    ) {
        $addressModel = $proceed($address);

        // Set all attributes
        $attributeValues = $this->dataProcessor
            ->buildOutputDataArray($address, AddressInterface::class);
        $attributeValues['location_type'] = $address->getLocationType();
        $attributeValues['nearest_landmark'] = $address->getNearestLandmark();
        foreach ($attributeValues as $attributeCode => $attributeData) {
            if ($attributeCode === 'extension_attributes') {
                if (isset($attributeData['location_type'])){
                    $addressModel->setLocationType($attributeData['location_type']);
                    $this->request->setParam('location_type', $attributeData['location_type']);
                }
                if(isset($attributeData['nearest_landmark'])){
                    $addressModel->setNearestLandmark($attributeValues['nearest_landmark']);
                    $this->request->setParam('nearest_landmark', $attributeData['nearest_landmark']);
                }
            }
        }
        return $addressModel;
    }

    /**
     * @param $address
     * @return mixed
     */
    protected function _getAddressParams($address)
    {
        $addresses = $this->request->getParam('address');
        if (!$addresses) {
            return [];
        }

        $entityId = $address->getId();
        if ($entityId && !empty($addresses[$entityId])) {
            return $addresses[$entityId];
        }

        $index = $this->request->getParam('currentNewIndex') ?: 0;
        if (!$entityId && !empty($addresses['new_' . $index])) {
            $this->request->setParam('currentNewIndex', $index + 1);
            return $addresses['new_' . $index];
        }

        return [];
    }
}