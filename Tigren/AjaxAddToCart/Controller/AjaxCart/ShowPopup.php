<?php

namespace Tigren\AjaxAddToCart\Controller\AjaxCart;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;

/**
 * Class ShowPopup
 * @package Tigren\AjaxAddToCart\Controller\AjaxCart
 */
class ShowPopup extends Action
{

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var LayoutFactory
     */
    protected $_layoutFactory;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var ProductRepository
     */
    protected $productRepositoty;


    /**
     * ShowPopup constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Registry $registry
     * @param ProductRepository $productRepository
     * @param LayoutFactory $_layoutFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Registry $registry,
        ProductRepository $productRepository,
        LayoutFactory $_layoutFactory
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->registry = $registry;
        $this->productRepositoty = $productRepository;
        $this->_layoutFactory = $_layoutFactory;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $params = $this->getRequest()->getParams();
        $productId = $this->getRequest()->getParam('product');
        $product = $this->productRepositoty->getById($productId);
        $this->registry->register('product', $product);
        $this->registry->register('current_product', $product);
        $ajax_success = $this->getRequest()->getParam('ajax_success');

        if ($ajax_success == 1) {
            $result = [
                'content' => $this->getHtmlPopupSuccess(),
                'message' => __("SUCCESS")
            ];
            $resultJson->setData($result);
            return $resultJson;
        }

        $ajax_error = $this->getRequest()->getParam('ajax_error');
        if ($ajax_error == 1) {
            $result = [
                'content' => $this->getHtmlPopupError(),
                'message' => __("ERROR"),
            ];
            $resultJson->setData($result);
            return $resultJson;
        }
        $productType = $product->getTypeId();

        //check option config product
        $configOption = $this->getRequest()->getParam('super_attribute');
        if ($productType == 'configurable') {
            foreach ($configOption as $key => $value) {
                if (!$value) {
                    $result = [
                        'content' => $this->getConfigOptionHtml(),
                        'message' => false
                    ];
                    $resultJson->setData($result);
                    return $resultJson;
                    break;
                }
            }
        }

        //check option bundle product
        $bundleOption = $this->getRequest()->getParam('bundle_option');

        if (!$bundleOption && $productType == "bundle") {
            $result = [
                'content' => $this->getBundleOptionHtml(),
                'message' => false
            ];
            $resultJson->setData($result);
            return $resultJson;
        }

        //check downloadable product to show popup
        $Qty = $this->getRequest()->getParam('qty');
        if ($productType == "downloadable" && !$Qty) {
            $result = [
                'content' => $this->getPopupDownloadHtml(),
                'message' => false,
            ];
            $resultJson->setData($result);
            return $resultJson;
        }

        //check group product to show popup
        $superGroup = $this->getRequest()->getParam('super_group');
        if ($productType == "grouped") {
            if (!$superGroup) {
                $result = [
                    'content' => $this->getPopupGroupOptionHtml(),
                    'message' => false
                ];
                $resultJson->setData($result);
                return $resultJson;
            } else {
                $sum = 0;
                foreach ($superGroup as $key => $value) {
                    $sum += $value;
                };
                if ($value == 0) {
                    $result = [
                        'content' => $this->getPopupGroupOptionHtml(),
                        'message' => false
                    ];
                    $resultJson->setData($result);
                    return $resultJson;
                }
            }
        }

        if ($productId) {
            $this->_forward('add', 'cart', 'checkout', $params);
        }
    }

    /**
     * @return string
     */
    public function getHtmlPopupSuccess()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_product_success')->load();
        } catch (LocalizedException $e) {
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     */
    public function getHtmlPopupError()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_product_error')->load();
        } catch (LocalizedException $e) {
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     */
    public function getConfigOptionHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_config_product_option')->load();
        } catch (LocalizedException $e) {
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     */
    public function getBundleOptionHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_bundle_product_option')->load();
        } catch (LocalizedException $e) {
            die($e->getMessage());
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     */
    public function getPopupDownloadHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_downloadable_product')->load();
        } catch (LocalizedException $e) {
            die($e->getMessage());
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     */
    public function getPopupGroupOptionHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        try {
            $layout->getUpdate()->addHandle('popup_catalog_group_product_option')->load();
        } catch (LocalizedException $e) {
        }
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }
}