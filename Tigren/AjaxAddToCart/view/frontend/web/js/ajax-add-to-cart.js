define([
    "jquery",
    "mage/translate"
], function ($) {
    "use strict";
    $.widget('addtocart.ajax', {

        _create: function () {
            this._bind();
        },

        _bind: function () {
            var self = this;
            self.element.on('click', function (e) {
                e.preventDefault();
                self._ajaxSubmit($(this));
            });
        },

        _ajaxSubmit: function (element) {
            var params = element.parents('form').serializeArray();
            var ajaxUrl = this.options.url;
            $('body').trigger('processStart');
            $.ajax({
                url: ajaxUrl,
                data: params,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    //check return request1 is empty then show popup sucess
                    if (res.length === 0) {
                        params.push({name: 'ajax_success', value: '1'});
                        $.ajax({
                            url: ajaxUrl,
                            data: params,
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                if (response.content) {
                                    $(".tg-message-success").html(response.message);
                                    $("#ajaxresponse").html(response.content).show();
                                }
                                $('body').trigger('contentUpdated').trigger('processStop');
                            }
                        })
                    }

                    //check return request 1 is backUrl when show popup error
                    if (res.backUrl) {
                        params.push({name: 'ajax_error', value: '1'});
                        var errorMessageInterval = setInterval(function () {
                            var messageElm = $('.page.messages .message-error ').children(":first");
                            if (messageElm.length > 0 && messageElm.text() !== '') {
                                clearInterval(errorMessageInterval);
                                $('#tg-popup-error').find('.tg-error-message').text(messageElm.text());
                            }
                        }, 500);
                        $.ajax({
                            url: ajaxUrl,
                            data: params,
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                if (response.content) {
                                    $("#ajaxresponse").html(response.content).show();
                                }
                                $('body').trigger('contentUpdated').trigger('processStop');
                            }
                        })
                    }

                    //show popup option of config product
                    if (res.message === false) {
                        $("#ajaxresponse").html(res.content).show();
                        $('body').trigger('contentUpdated').trigger('processStop');
                    }
                },
            });
        },
    });
    return $.addtocart.ajax;
});