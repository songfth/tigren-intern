<?php

namespace Tigren\AjaxAddToCart\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Cart;

/**
 * Class InforCart
 * @package Tigren\AjaxAddToCart\Block\Product
 */
class CartInfor extends Template
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * InforCart constructor.
     * @param Context $context
     * @param Cart $cart
     */
    public function __construct(Context $context, Cart $cart)
    {
        $this->cart = $cart;
        parent::__construct($context);
    }

    /**
     * @return float|mixed|null
     */
    public function getTotalQuantity()
    {
        return $this->cart->getItemsQty();
    }

    /**
     * @return float
     */
    public function getSubTotalPrice()
    {
        $grandTotal = $this->cart->getQuote()->getSubtotal();
        return $grandTotal;
    }
}