<?php

namespace Tigren\AjaxAddToCart\Block\Product\Renderer;

use Magento\Swatches\Block\Product\Renderer\Configurable;

/**
 * Class SwatchConfig
 * @package Tigren\AjaxAddToCart\Block\Product\Renderer
 */
class SwatchConfig extends Configurable
{
    const SWATCH_RENDERER_TEMPLATE = 'Tigren_AjaxAddToCart::product/view/renderer.phtml';

    /**
     * @return string
     */
    protected function getRendererTemplate()
    {
        return $this->isProductHasSwatchAttribute() ?
            self::SWATCH_RENDERER_TEMPLATE : self::CONFIGURABLE_RENDERER_TEMPLATE;
    }

}
