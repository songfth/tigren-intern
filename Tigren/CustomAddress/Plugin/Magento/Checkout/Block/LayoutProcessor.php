<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Plugin\Magento\Checkout\Block;

use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Checkout\Helper\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\Quote;
use Magento\Store\Api\StoreResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\AttributeMapper;
use Tigren\CustomAddress\Helper\Data as CustomAddressHelper;
use Tigren\CustomAddress\Model\Config\Source\SuggestionType;
use Tigren\CustomAddress\Model\ResourceModel\Subdistrict\CollectionFactory;

/**
 * Class LayoutProcessor
 *
 * @package Tigren\CustomAddress\Plugin\Magento\Checkout\Block
 */
class LayoutProcessor
{
    /**
     * @var CheckoutSession
     */
    public $checkoutSession;

    /**
     * @var null
     */
    public $quote = null;

    /**
     * @var AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var AttributeMerger
     */
    protected $merger;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var array
     */
    private $cityOptions;

    /**
     * @var array
     */
    private $subdistrictOptions;

    /**
     * @var \Tigren\CustomAddress\Model\ResourceModel\City\CollectionFactory
     */
    private $cityCollectionFactory;

    /**
     * @var CollectionFactory
     */
    private $subdistrictCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DirectoryHelper
     */
    private $directoryHelper;

    /**
     * @var CustomAddressHelper
     */
    private $customAddressHelper;

    /**
     * @var Data
     */
    private $checkoutDataHelper;

    /**
     * LayoutProcessor constructor.
     *
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     * @param CheckoutSession $checkoutSession
     * @param UrlInterface $urlBuilder
     * @param \Tigren\CustomAddress\Model\ResourceModel\City\CollectionFactory $cityCollection
     * @param CollectionFactory $subdistrictCollection
     * @param StoreResolverInterface $storeResolver @deprecated
     * @param DirectoryHelper $directoryHelper
     * @param CustomAddressHelper $customAddressHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        CheckoutSession $checkoutSession,
        UrlInterface $urlBuilder,
        \Tigren\CustomAddress\Model\ResourceModel\City\CollectionFactory $cityCollection,
        CollectionFactory $subdistrictCollection,
        StoreResolverInterface $storeResolver,
        DirectoryHelper $directoryHelper,
        CustomAddressHelper $customAddressHelper,
        StoreManagerInterface $storeManager = null
    ) {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->checkoutSession = $checkoutSession;
        $this->urlBuilder = $urlBuilder;
        $this->cityCollectionFactory = $cityCollection;
        $this->subdistrictCollectionFactory = $subdistrictCollection;
        $this->directoryHelper = $directoryHelper;
        $this->customAddressHelper = $customAddressHelper;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param  $result
     * @param array $jsLayout
     * @return mixed
     * @throws LocalizedException
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        $result,
        array $jsLayout
    ) {
        $jsLayoutResult = $result;

        if ($this->getQuote()->isVirtual()) {
            return $jsLayoutResult;
        }

        // Add dictionaries
        if (!isset($jsLayoutResult['components']['checkoutProvider']['additional_dictionaries'])) {
            $jsLayoutResult['components']['checkoutProvider']['additional_dictionaries']['city_id'] = $this->getCityOptions();
            $jsLayoutResult['components']['checkoutProvider']['additional_dictionaries']['subdistrict_id'] = $this->getSubdistrictOptions();
        }

        $addressElements = $this->getAddressAttributes();
        if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])
        ) {
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset'] = $this->getShippingAddressComponent($addressElements);
        }

        // Billing address at step shipping or payment
        if ($this->customAddressHelper->getMoveBilling()) {
            $jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['billingAddress']['children']['address-fieldset'] = $this->getBillingAddressComponentAtShipping($addressElements);

            if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form'])
            ) {
                unset(
                    $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['afterMethods']['children']['billing-address-form']
                );
            }

            if ($billingAddressForms = $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']                ['payment']['children']['payments-list']['children']
            ) {
                foreach ($billingAddressForms as $billingAddressFormsKey => $billingAddressForm) {
                    if ($billingAddressFormsKey != 'before-place-order') {
                        unset(
                            $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']
                            ['payment']['children']['payments-list']['children'][$billingAddressFormsKey]
                        );
                    }
                }
            }
        } else {
            if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children'])
            ) {
                $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children'] = $this->processPaymentChildrenComponents(
                    $jsLayoutResult['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children'],
                    $addressElements
                );
            }
            if (isset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['billingAddress'])) {
                unset($jsLayoutResult['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['billingAddress']);
            }
        }

        return $jsLayoutResult;
    }

    /**
     * Get Quote
     *
     * @return Quote|null
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }

        return $this->quote;
    }

    /**
     * Get city options list.
     *
     * @return array
     */
    private function getCityOptions()
    {
        if (!isset($this->cityOptions)) {
            $this->cityOptions = $this->cityCollectionFactory->create()->toOptionArray();
        }

        return $this->cityOptions;
    }

    /**
     * Get sub district options list.
     *
     * @return array
     */
    private function getSubdistrictOptions()
    {
        if (!isset($this->subdistrictOptions)) {
            $this->subdistrictOptions = $this->subdistrictCollectionFactory->create()->toOptionArray();
        }

        return $this->subdistrictOptions;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    private function getAddressAttributes()
    {
        /**
         * @var AttributeInterface[] $attributes
         */
        $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
            'customer_address',
            'customer_register_address'
        );

        $elements = [];
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            if ($attribute->getIsUserDefined()) {
                continue;
            }
            $elements[$code] = $this->attributeMapper->map($attribute);
            if (isset($elements[$code]['label'])) {
                $label = $elements[$code]['label'];
                $elements[$code]['label'] = __($label);
            }
        }

        return $elements;
    }

    /**
     * Prepare shipping address field for shipping step for physical product
     *
     * @param  $elements
     * @return array
     */
    public function getShippingAddressComponent($elements)
    {
        $providerName = 'checkoutProvider';
        $suggestionType = $this->customAddressHelper->getSuggestionType();

        if ($suggestionType == SuggestionType::SUGGESTION_TYPE_DROP_DOWN) {
            $components = [
                'component' => 'uiComponent',
                'displayArea' => 'additional-fieldsets',
                'children' => $this->merger->merge(
                    $elements,
                    $providerName,
                    'shippingAddress',
                    [
                        'firstname' => [
                            'visible' => true,
                        ],
                        'lastname' => [
                            'visible' => true,
                        ],
                        'country_id' => [
                            'sortOrder' => 200
                        ],
                        'region' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.region',
                            'label' => __('State/Region'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'sortOrder' => 210
                        ],
                        'region_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/region',
                            'config' => [
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'customEntry' => 'shippingAddress.region',
                                'additionalClasses' => 'shipping-region'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                'field' => 'country_id'
                            ],
                            'sortOrder' => 220
                        ],
                        'city' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.city',
                            'label' => __('City'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 230
                        ],
                        'city_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/city',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'additionalClasses' => 'shipping-city'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:shippingAddress.region_id',
                                'field' => 'region_id'
                            ],
                            'sortOrder' => 240
                        ],
                        'subdistrict' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.custom_attributes.subdistrict',
                            'label' => __('Subdistrict'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 250
                        ],
                        'subdistrict_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/subdistrict',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'additionalClasses' => 'shipping-subdistrict'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                'field' => 'city_id'
                            ],
                            'sortOrder' => 260
                        ],
                        'postcode' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/post-code',
                            'config' => [
                                'customEntry' => 'shippingAddress.postcode',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input',
                                'additionalClasses' => 'shipping-postcode'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 270
                        ],
                        'company' => [
                            'visible' => 0,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 70
                        ],
                        'fax' => [
                            'visible' => false,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 290
                        ],
                        'telephone' => [
                            'visible' => 1,
                            'config' => [
                                'tooltip' => [
                                    'description' => __('For delivery questions.'),
                                ]
                            ],
                            'label' => __('Telephone'),
                            'sortOrder' => 60
                        ],
                        'type' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.type',
                            'label' => __('Type'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'value' => 'new-shipping-address',
                            'sortOrder' => 300
                        ]
                    ]
                )
            ];
        } else {
            $components = [
                'component' => 'uiComponent',
                'displayArea' => 'additional-fieldsets',
                'children' => $this->merger->merge(
                    $elements,
                    $providerName,
                    'shippingAddress',
                    [
                        'firstname' => [
                            'visible' => true,
                        ],
                        'lastname' => [
                            'visible' => true,
                        ],
                        'country_id' => [
                            'sortOrder' => 200
                        ],
                        'subdistrict' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.custom_attributes.subdistrict',
                            'label' => __('City'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 210
                        ],
                        'subdistrict_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/subdistrict',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'shippingAddress.subdistrict',
                                'additionalClasses' => 'shipping-subdistrict'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                'field' => 'city_id'
                            ],
                            'sortOrder' => 220
                        ],
                        'city' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.city',
                            'label' => __('City'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 230
                        ],
                        'city_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/city',
                            'config' => [
                                'customScope' => 'shippingAddress.custom_attributes',
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'shippingAddress.city',
                                'additionalClasses' => 'shipping-city'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.region_id',
                                'field' => 'region_id'
                            ],
                            'sortOrder' => 240
                        ],
                        'region' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.region',
                            'label' => __('State/Region'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'sortOrder' => 250
                        ],
                        'region_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/region',
                            'config' => [
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'shippingAddress.region',
                                'additionalClasses' => 'shipping-region'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                'field' => 'country_id'
                            ],
                            'sortOrder' => 260
                        ],
                        'postcode' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/post-code',
                            'config' => [
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'shippingAddress.postcode',
                                'additionalClasses' => 'shipping-postcode'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 270
                        ],
                        'company' => [
                            'visible' => 0,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 70
                        ],
                        'fax' => [
                            'visible' => false,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 290
                        ],
                        'telephone' => [
                            'visible' => 1,
                            'config' => [
                                'tooltip' => [
                                    'description' => __('For delivery questions.'),
                                ]
                            ],
                            'label' => __('Telephone'),
                            'sortOrder' => 60
                        ],
                        'type' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'shippingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'shippingAddress.type',
                            'label' => __('Type'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'value' => 'new-shipping-address',
                            'sortOrder' => 300
                        ]
                    ]
                )
            ];
        }

        if (isset($components['children'])) {
            foreach ($components['children'] as $key => $value) {
                $components['children'][$key]['config']['additionalClasses'] = 'shipping-' . $key;

                if ($key === 'city_id') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key,
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key
                    ];
                } elseif ($key === 'subdistrict_id' || $key === 'postcode') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id',
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id'
                    ];
                }

                if ($key === 'city_id' || $key === 'subdistrict_id' || $key === 'postcode') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];

                    if ($key === 'city_id' || $key === 'subdistrict_id') {
                        $components['children'][$key]['dataScope'] = 'shippingAddress.custom_attributes.' . $key;
                    }
                }
            }
        }

        return $components;
    }

    /**
     * Prepare billing address field for shipping step for physical product
     *
     * @param  $elements
     * @return array
     */
    public function getBillingAddressComponentAtShipping($elements)
    {
        $providerName = 'checkoutProvider';
        $suggestionType = $this->customAddressHelper->getSuggestionType();

        if ($suggestionType == SuggestionType::SUGGESTION_TYPE_DROP_DOWN) {
            $components = [
                'component' => 'uiComponent',
                'displayArea' => 'additional-fieldsets',
                'children' => $this->merger->merge(
                    $elements,
                    $providerName,
                    'billingAddress',
                    [
                        'firstname' => [
                            'visible' => true,
                        ],
                        'lastname' => [
                            'visible' => true,
                        ],
                        'country_id' => [
                            'sortOrder' => 200
                        ],
                        'region' => [
                            'visible' => false,
                            'sortOrder' => 210
                        ],
                        'region_id' => [
                            'component' => 'Magento_Ui/js/form/element/region',
                            'config' => [
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'customEntry' => 'billingAddress.region',
                                'additionalClasses' => 'billing-region'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                'field' => 'country_id'
                            ],
                            'sortOrder' => 220
                        ],
                        'city' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.city',
                            'label' => __('City'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 230
                        ],
                        'city_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/city',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'additionalClasses' => 'billing-city'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:billingAddress.region_id',
                                'field' => 'region_id'
                            ],
                            'sortOrder' => 240
                        ],
                        'subdistrict' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.custom_attributes.subdistrict',
                            'label' => __('Subdistrict'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 250
                        ],
                        'subdistrict_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/subdistrict',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/select',
                                'additionalClasses' => 'billing-subdistrict'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                'field' => 'city_id'
                            ],
                            'sortOrder' => 260
                        ],
                        'postcode' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/post-code',
                            'config' => [
                                'customEntry' => 'billingAddress.postcode',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input',
                                'additionalClasses' => 'billing-postcode'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 270
                        ],
                        'company' => [
                            'visible' => 0,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 70
                        ],
                        'fax' => [
                            'visible' => false,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 290
                        ],
                        'telephone' => [
                            'visible' => 1,
                            'config' => [
                                'tooltip' => [
                                    'description' => __('For delivery questions.'),
                                ]
                            ],
                            'label' => __('Telephone'),
                            'sortOrder' => 60
                        ],
                        'type' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.type',
                            'label' => __('Type'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'value' => 'new-billing-address',
                            'sortOrder' => 300
                        ]
                    ]
                )
            ];
        } else {
            $components = [
                'component' => 'uiComponent',
                'displayArea' => 'additional-fieldsets',
                'children' => $this->merger->merge(
                    $elements,
                    $providerName,
                    'billingAddress',
                    [
                        'firstname' => [
                            'visible' => true,
                        ],
                        'lastname' => [
                            'visible' => true,
                        ],
                        'country_id' => [
                            'sortOrder' => 200
                        ],
                        'subdistrict' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.custom_attributes.subdistrict',
                            'label' => __('Subdistrict'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 210
                        ],
                        'subdistrict_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/subdistrict',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'billingAddress.subdistrict',
                                'additionalClasses' => 'billing-subdistrict'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                'field' => 'city_id'
                            ],
                            'sortOrder' => 220
                        ],
                        'city' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.city',
                            'label' => __('City'),
                            'provider' => 'checkoutProvider',
                            'validation' => [
                                'required-entry' => 1,
                                'max_text_length' => 255,
                                'min_text_length' => 1
                            ],
                            'visible' => false,
                            'sortOrder' => 230
                        ],
                        'city_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/city',
                            'config' => [
                                'customScope' => 'billingAddress.custom_attributes',
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'billingAddress.city',
                                'additionalClasses' => 'billing-city'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.region_id',
                                'field' => 'region_id'
                            ],
                            'sortOrder' => 240
                        ],
                        'region' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.region',
                            'label' => __('State/Region'),
                            'provider' => 'billingAddress',
                            'visible' => false,
                            'sortOrder' => 250
                        ],
                        'region_id' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/region',
                            'config' => [
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'billingAddress.region',
                                'additionalClasses' => 'billing-region'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'filterBy' => [
                                'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                'field' => 'country_id'
                            ],
                            'sortOrder' => 260
                        ],
                        'postcode' => [
                            'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/post-code',
                            'config' => [
                                'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                'customEntry' => 'billingAddress.postcode',
                                'additionalClasses' => 'billing-postcode'
                            ],
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 270
                        ],
                        'company' => [
                            'visible' => 0,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 70
                        ],
                        'fax' => [
                            'visible' => false,
                            'validation' => [
                                'min_text_length' => 0
                            ],
                            'sortOrder' => 290
                        ],
                        'telephone' => [
                            'visible' => 1,
                            'config' => [
                                'tooltip' => [
                                    'description' => __('For delivery questions.'),
                                ]
                            ],
                            'label' => __('Telephone'),
                            'sortOrder' => 60
                        ],
                        'type' => [
                            'component' => 'Magento_Ui/js/form/element/abstract',
                            'config' => [
                                'customScope' => 'billingAddress',
                                'template' => 'ui/form/field',
                                'elementTmpl' => 'ui/form/element/input'
                            ],
                            'dataScope' => 'billingAddress.type',
                            'label' => __('Type'),
                            'provider' => 'checkoutProvider',
                            'visible' => false,
                            'value' => 'new-billing-address',
                            'sortOrder' => 300
                        ]
                    ]
                )
            ];
        }

        if (isset($components['children'])) {
            foreach ($components['children'] as $key => $value) {
                $components['children'][$key]['config']['additionalClasses'] = 'billing-' . $key;

                if ($key === 'city_id') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key,
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key
                    ];
                } elseif ($key === 'subdistrict_id' || $key === 'postcode') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id',
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id'
                    ];
                }

                if ($key === 'city_id' || $key === 'subdistrict_id' || $key === 'postcode') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                }

                if ($key === 'city_id' || $key === 'subdistrict_id') {
                    $components['children'][$key]['dataScope'] = 'billingAddress.custom_attributes.' . $key;
                }
            }
        }

        return $components;
    }

    /**
     * Appends billing address form component to payment layout
     *
     * @param array $paymentLayout
     * @param array $elements
     * @return array
     */
    private function processPaymentChildrenComponents(array $paymentLayout, array $elements)
    {
        if (!isset($paymentLayout['payments-list']['children'])) {
            $paymentLayout['payments-list']['children'] = [];
        }

        if (!isset($paymentLayout['afterMethods']['children'])) {
            $paymentLayout['afterMethods']['children'] = [];
        }

        // The if billing address should be displayed on Payment method or page
        if ($this->getCheckoutDataHelper()->isDisplayBillingOnPaymentMethodAvailable()) {
            if (!empty($paymentLayout['payments-list']['children'])) {
                foreach (array_keys($paymentLayout['payments-list']['children']) as $key) {
                    if ($key == 'before-place-order') {
                        continue;
                    }
                    $paymentCode = substr($key, 0, -5);
                    if (isset($paymentLayout['payments-list']['children'][$key])) {
                        $paymentLayout['payments-list']['children'][$key] = $this->getBillingAddressComponentAtPayment(
                            $paymentCode,
                            $elements
                        );
                    }
                }
            }
        } else {
            if (isset($paymentLayout['afterMethods']['children']['billing-address-form'])) {
                unset($paymentLayout['afterMethods']['children']['billing-address-form']);
            }
            $component['billing-address-form'] = $this->getBillingAddressComponentAtPayment('shared', $elements);
            $paymentLayout['afterMethods']['children'] =
                array_merge_recursive(
                    $component,
                    $paymentLayout['afterMethods']['children']
                );
        }

        return $paymentLayout;
    }

    /**
     * Get checkout data helper instance
     *
     * @return     Data
     * @deprecated 100.1.4
     */
    private function getCheckoutDataHelper()
    {
        if (!$this->checkoutDataHelper) {
            $this->checkoutDataHelper = ObjectManager::getInstance()->get(Data::class);
        }

        return $this->checkoutDataHelper;
    }

    /**
     * Gets billing address component details
     *
     * @param string $paymentCode
     * @param array $elements
     * @return array
     */
    private function getBillingAddressComponentAtPayment($paymentCode, $elements)
    {
        $providerName = 'checkoutProvider';
        $suggestionType = $this->customAddressHelper->getSuggestionType();

        if ($suggestionType == SuggestionType::SUGGESTION_TYPE_DROP_DOWN) {
            $components = [
                'component' => 'Magento_Checkout/js/view/billing-address',
                'displayArea' => 'billing-address-form-' . $paymentCode,
                'provider' => 'checkoutProvider',
                'deps' => 'checkoutProvider',
                'dataScopePrefix' => 'billingAddress' . $paymentCode,
                'sortOrder' => 1,
                'children' => [
                    'form-fields' => [
                        'component' => 'uiComponent',
                        'displayArea' => 'additional-fieldsets',
                        'children' => $this->merger->merge(
                            $elements,
                            'checkoutProvider',
                            'billingAddress' . $paymentCode,
                            [
                                'firstname' => [
                                    'visible' => true,
                                ],
                                'lastname' => [
                                    'visible' => true,
                                    'sortOrder' => 10
                                ],
                                'country_id' => [
                                    'sortOrder' => 200
                                ],
                                'region' => [
                                    'visible' => false,
                                    'sortOrder' => 210
                                ],
                                'region_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/region',
                                    'config' => [
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/select',
                                        'customEntry' => 'billingAddress' . $paymentCode . '.region',
                                        'additionalClasses' => 'billing-region'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                        'field' => 'country_id'
                                    ],
                                    'sortOrder' => 220
                                ],
                                'city' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode,
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress' . $paymentCode . '.city',
                                    'label' => __('City'),
                                    'provider' => 'checkoutProvider',
                                    'validation' => [
                                        'required-entry' => 1,
                                        'max_text_length' => 255,
                                        'min_text_length' => 1
                                    ],
                                    'visible' => false,
                                    'sortOrder' => 230
                                ],
                                'city_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/city',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/select',
                                        'additionalClasses' => 'billing-city'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:billingAddress' . $paymentCode . '.region_id',
                                        'field' => 'region_id'
                                    ],
                                    'sortOrder' => 240
                                ],
                                'subdistrict' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress' . $paymentCode . '.custom_attributes.subdistrict',
                                    'label' => __('Subdistrict'),
                                    'provider' => 'checkoutProvider',
                                    'validation' => [
                                        'required-entry' => 1,
                                        'max_text_length' => 255,
                                        'min_text_length' => 1
                                    ],
                                    'visible' => false,
                                    'sortOrder' => 250
                                ],
                                'subdistrict_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/subdistrict',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/select',
                                        'additionalClasses' => 'billing-subdistrict'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                        'field' => 'city_id'
                                    ],
                                    'sortOrder' => 260
                                ],
                                'postcode' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/drop-down/directory/post-code',
                                    'config' => [
                                        'customEntry' => 'billingAddress.postcode',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input',
                                        'additionalClasses' => 'billing-postcode'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'sortOrder' => 270
                                ],
                                'company' => [
                                    'visible' => 0,
                                    'validation' => [
                                        'min_text_length' => 0
                                    ],
                                    'sortOrder' => 70
                                ],
                                'fax' => [
                                    'visible' => false,
                                    'validation' => [
                                        'min_text_length' => 0
                                    ],
                                    'sortOrder' => 290
                                ],
                                'telephone' => [
                                    'visible' => 1,
                                    'config' => [
                                        'tooltip' => [
                                            'description' => __('For delivery questions.'),
                                        ]
                                    ],
                                    'label' => __('Telephone'),
                                    'sortOrder' => 60
                                ],
                                'type' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress.type',
                                    'label' => __('Type'),
                                    'provider' => 'checkoutProvider',
                                    'visible' => false,
                                    'value' => 'new-billing-address',
                                    'sortOrder' => 300
                                ]
                            ]
                        ),
                    ],
                ],
            ];
        } else {
            $components = [
                'component' => 'Magento_Checkout/js/view/billing-address',
                'displayArea' => 'billing-address-form-' . $paymentCode,
                'provider' => 'checkoutProvider',
                'deps' => 'checkoutProvider',
                'dataScopePrefix' => 'billingAddress' . $paymentCode,
                'sortOrder' => 1,
                'children' => [
                    'form-fields' => [
                        'component' => 'uiComponent',
                        'displayArea' => 'additional-fieldsets',
                        'children' => $this->merger->merge(
                            $elements,
                            'checkoutProvider',
                            'billingAddress' . $paymentCode,
                            [
                                'firstname' => [
                                    'visible' => true,
                                ],
                                'lastname' => [
                                    'visible' => true,
                                ],
                                'country_id' => [
                                    'sortOrder' => 200
                                ],
                                'subdistrict' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress' . $paymentCode . '.custom_attributes.subdistrict',
                                    'label' => __('Subdistrict'),
                                    'provider' => 'checkoutProvider',
                                    'validation' => [
                                        'required-entry' => 1,
                                        'max_text_length' => 255,
                                        'min_text_length' => 1
                                    ],
                                    'visible' => false,
                                    'sortOrder' => 210
                                ],
                                'subdistrict_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/subdistrict',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                        'customEntry' => 'billingAddress' . $paymentCode . '.subdistrict',
                                        'additionalClasses' => 'billing-subdistrict'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:${ $.parentScope }.city_id',
                                        'field' => 'city_id'
                                    ],
                                    'sortOrder' => 220
                                ],
                                'city' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode,
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress' . $paymentCode . 'city',
                                    'label' => __('City'),
                                    'provider' => 'checkoutProvider',
                                    'validation' => [
                                        'required-entry' => 1,
                                        'max_text_length' => 255,
                                        'min_text_length' => 1
                                    ],
                                    'visible' => false,
                                    'sortOrder' => 230
                                ],
                                'city_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/city',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode . '.custom_attributes',
                                        'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                        'customEntry' => 'billingAddress.city',
                                        'additionalClasses' => 'billing-city'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:${ $.parentScope }' . $paymentCode . '.region_id',
                                        'field' => 'region_id'
                                    ],
                                    'sortOrder' => 240
                                ],
                                'region' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress' . $paymentCode,
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress' . $paymentCode . 'region',
                                    'label' => __('State/Region'),
                                    'provider' => 'checkoutProvider',
                                    'visible' => false,
                                    'sortOrder' => 250
                                ],
                                'region_id' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/region',
                                    'config' => [
                                        'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                        'customEntry' => 'billingAddress' . $paymentCode . '.region',
                                        'additionalClasses' => 'billing-region'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'filterBy' => [
                                        'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                        'field' => 'country_id'
                                    ],
                                    'sortOrder' => 260
                                ],
                                'postcode' => [
                                    'component' => 'Tigren_CustomAddress/js/form/element/auto-complete/directory/post-code',
                                    'config' => [
                                        'template' => 'Tigren_CustomAddress/form/element/auto-complete/directory/ui-select',
                                        'customEntry' => 'billingAddress.postcode',
                                        'additionalClasses' => 'billing-postcode'
                                    ],
                                    'validation' => [
                                        'required-entry' => true
                                    ],
                                    'sortOrder' => 270
                                ],
                                'company' => [
                                    'visible' => 0,
                                    'validation' => [
                                        'min_text_length' => 0
                                    ],
                                    'sortOrder' => 70
                                ],
                                'fax' => [
                                    'visible' => false,
                                    'validation' => [
                                        'min_text_length' => 0
                                    ],
                                    'sortOrder' => 290
                                ],
                                'telephone' => [
                                    'visible' => 1,
                                    'config' => [
                                        'tooltip' => [
                                            'description' => __('For delivery questions.'),
                                        ]
                                    ],
                                    'label' => __('Telephone'),
                                    'sortOrder' => 60
                                ],
                                'type' => [
                                    'component' => 'Magento_Ui/js/form/element/abstract',
                                    'config' => [
                                        'customScope' => 'billingAddress',
                                        'template' => 'ui/form/field',
                                        'elementTmpl' => 'ui/form/element/input'
                                    ],
                                    'dataScope' => 'billingAddress.type',
                                    'label' => __('Type'),
                                    'provider' => 'checkoutProvider',
                                    'visible' => false,
                                    'value' => 'new-billing-address',
                                    'sortOrder' => 300
                                ]
                            ]
                        ),
                    ],
                ],
            ];
        }

        if (isset($components['children']['form-fields']['children'])) {
            foreach ($components['children']['form-fields']['children'] as $key => $value) {
                $components['children']['form-fields']['children'][$key]['config']['additionalClasses'] = 'billing-' . $key;

                if ($key === 'city_id') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key,
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.' . $key
                    ];
                } elseif ($key === 'subdistrict_id' || $key === 'postcode') {
                    unset($components['children'][$key]['options']);
                    $components['children'][$key]['deps'] = [$providerName];
                    $components['children'][$key]['imports'] = [
                        'initialOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id',
                        'setOptions' => 'index = ' . $providerName . ':additional_dictionaries.subdistrict_id'
                    ];
                }

                if ($key === 'city_id' || $key === 'subdistrict_id') {
                    $components['children']['form-fields']['children'][$key]['dataScope'] = 'billingAddress' . $paymentCode . '.custom_attributes.' . $key;
                }

                if ($key === 'postcode') {
                    $components['children']['form-fields']['children'][$key]['config']['suggestType'] = $suggestionType;
                }
            }
        }

        return $components;
    }
}
