<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Plugin\Magento\Checkout\Model;

use Magento\Customer\Model\AddressFactory;
use Tigren\CustomAddress\Helper\Data as CustomAddressHelper;

/**
 * Class DefaultConfigProvider
 *
 * @package Tigren\CustomAddress\Plugin\Magento\Checkout\Model
 */
class DefaultConfigProvider
{
    /**
     * @var AddressFactory
     */
    protected $addressFactory;

    /**
     * @var CustomAddressHelper
     */
    private $customAddressHelper;

    /**
     * DefaultConfigProvider constructor.
     *
     * @param AddressFactory      $addressFactory
     * @param CustomAddressHelper $customAddressHelper
     */
    public function __construct(
        AddressFactory $addressFactory,
        CustomAddressHelper $customAddressHelper
    ) {
        $this->addressFactory = $addressFactory;
        $this->customAddressHelper = $customAddressHelper;
    }

    /**
     * @param  \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param  $result
     * @return mixed
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, $result)
    {
        // custom_attribute for address
        if (!empty($result['customerData']['addresses'])) {
            foreach ($result['customerData']['addresses'] as &$address) {
                $custom_attributes = [];
                $addressModel = $this->addressFactory->create()->load($address['id']);
                $fields = ['city_id', 'subdistrict', 'subdistrict_id'];
                foreach ($fields as $field) {
                    if ($addressModel->getData($field)) {
                        $custom_attributes[$field] = $addressModel->getData($field);
                    }
                }
                if (!empty($custom_attributes)) {
                    $address['custom_attributes'] = $custom_attributes;
                }
            }
        }
        $result['quoteData']['move_billing'] = $this->customAddressHelper->getMoveBilling();
        $result['quoteData']['suggestionType'] = $this->customAddressHelper->getSuggestionType();

        return $result;
    }
}
