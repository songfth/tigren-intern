<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Helper;

use Magento\Customer\Model\AddressFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Profiler;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Tigren\CustomAddress\Model\City;
use Tigren\CustomAddress\Model\CityFactory;
use Tigren\CustomAddress\Model\Config\Source\SuggestionType;
use Tigren\CustomAddress\Model\ResourceModel\Subdistrict\CollectionFactory;
use Tigren\CustomAddress\Model\SubdistrictFactory;

/**
 * Class Data
 *
 * @package Tigren\CustomAddress\Helper
 */
class Data extends \Magento\Directory\Helper\Data
{
    /**
     * Suggestion type configuration path
     */
    const XML_PATH_SUGGESTION_TYPE = 'custom_address/general/suggestion_type';

    /**
     * Move billing configuration path
     */
    const XML_PATH_MOVE_BILLING = 'custom_address/general/move_billing';

    /**
     * Json representation of cities data
     *
     * @var string
     */
    protected $_cityJson;

    /**
     * Region collection
     *
     * @var \Tigren\CustomAddress\Model\ResourceModel\City\Collection
     */
    protected $_cityCollection;

    /**
     * Json representation of subdistricts data
     *
     * @var string
     */
    protected $_subdistrictJson;

    /**
     * Region collection
     *
     * @var \Tigren\CustomAddress\Model\ResourceModel\Subdistrict\Collection
     */
    protected $_subdistrictCollection;

    /**
     * @var CityFactory
     */
    protected $cityFactory;

    /**
     * @var SubdistrictFactory
     */
    protected $subdistrictFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionFactory;

    /**
     * Currently selected store ID if applicable
     *
     * @var int
     */
    protected $_storeId;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var AddressFactory
     */
    protected $_customerAddress;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param Config $configCacheType
     * @param Collection $countryCollection
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regCollectionFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param StoreManagerInterface $storeManager
     * @param CurrencyFactory $currencyFactory
     * @param \Tigren\CustomAddress\Model\ResourceModel\City\CollectionFactory $cityCollectionFactory
     * @param CollectionFactory $subdistrictCollectionFactory
     * @param CityFactory $cityFactory
     * @param SubdistrictFactory $subdistrictFactory
     * @param RegionFactory $regionFactory
     * @param RequestInterface $request
     * @param AddressFactory $customerAddress
     * @throws NoSuchEntityException
     */
    public function __construct(
        Context $context,
        Config $configCacheType,
        Collection $countryCollection,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regCollectionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        StoreManagerInterface $storeManager,
        CurrencyFactory $currencyFactory,
        \Tigren\CustomAddress\Model\ResourceModel\City\CollectionFactory $cityCollectionFactory,
        CollectionFactory $subdistrictCollectionFactory,
        CityFactory $cityFactory,
        SubdistrictFactory $subdistrictFactory,
        RegionFactory $regionFactory,
        RequestInterface $request,
        AddressFactory $customerAddress
    ) {
        parent::__construct(
            $context,
            $configCacheType,
            $countryCollection,
            $regCollectionFactory,
            $jsonHelper,
            $storeManager,
            $currencyFactory
        );
        $this->_cityCollection = $cityCollectionFactory;
        $this->_subdistrictCollection = $subdistrictCollectionFactory;
        $this->cityFactory = $cityFactory;
        $this->subdistrictFactory = $subdistrictFactory;
        $this->regionFactory = $regionFactory;
        $this->_request = $request;
        $this->_customerAddress = $customerAddress;
        $this->setStoreId($this->getCurrentStoreId());
    }

    /**
     * Set a specified store ID value
     *
     * @param int $store
     * @return $this
     */
    public function setStoreId($store)
    {
        $this->_storeId = $store;
        return $this;
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    public function getCurrentStoreId()
    {
        return $this->_storeManager->getStore(true)->getId();
    }

    /**
     * Get Suggestion Type of Address
     *
     * @return int
     */
    public function getSuggestionType()
    {
        $suggestionType = (int)$this->getScopeConfig(self::XML_PATH_SUGGESTION_TYPE);

        return $suggestionType
            ? $suggestionType
            : SuggestionType::SUGGESTION_TYPE_AUTO_COMPLETE;
    }

    /**
     * @param  $path
     * @return mixed
     */
    public function getScopeConfig($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->_storeId);
    }

    /**
     * Get Move Billing At Checkout Page
     *
     * @return int
     */
    public function getMoveBilling()
    {
        return (int)$this->getScopeConfig(self::XML_PATH_MOVE_BILLING);
    }

    /**
     * Retrieve cities data json
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getCityJson()
    {
        Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);

        if (!$this->_cityJson) {
            $cacheKey = 'DIRECTORY_CITIES_JSON_STORE' . $this->_storeManager->getStore()->getId();
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $cities = $this->getCityData();
                $json = $this->jsonHelper->jsonEncode($cities);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_cityJson = $json;
        }

        Profiler::stop('TEST: ' . __METHOD__);

        return $this->_cityJson;
    }

    /**
     * Retrieve cities data
     *
     * @return array
     */
    public function getCityData()
    {
        $collection = $this->_cityCollection->create();

        $cities = [
            'config' => [
                'show_all_cities' => true
            ]
        ];

        foreach ($collection as $city) {
            /**
             * @var $city City
             */
            if (!$city->getCityId()) {
                continue;
            }
            $cities[$city->getRegionId()][$city->getCityId()] = [
                'code' => $city->getCode(),
                'name' => (string)__($city->getName()),
            ];
        }

        return $cities;
    }

    /**
     * Retrieve subdistricts data json
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getSubdistrictJson()
    {
        Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);

        if (!$this->_subdistrictJson) {
            $cacheKey = 'DIRECTORY_SUBDISTRICTS_JSON_STORE' . $this->_storeManager->getStore()->getId();
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $subdistricts = $this->getSubdistrictData();
                $json = $this->jsonHelper->jsonEncode($subdistricts);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_subdistrictJson = $json;
        }

        Profiler::stop('TEST: ' . __METHOD__);

        return $this->_subdistrictJson;
    }

    /**
     * Retrieve subdistrict data
     *
     * @return array
     */
    public function getSubdistrictData()
    {
        $collection = $this->_subdistrictCollection->create();

        $subdistricts = [
            'config' => [
                'show_all_subdistricts' => true
            ]
        ];

        foreach ($collection as $subdistrict) {
            /**
             * @var $subdistrict
             */
            if (!$subdistrict->getSubdistrictId()) {
                continue;
            }
            $subdistricts[$subdistrict->getCityId()][$subdistrict->getSubdistrictId()] = [
                'code' => $subdistrict->getCode(),
                'name' => (string)__($subdistrict->getName()),
                'zipcode' => $subdistrict->getZipcode()
            ];
        }

        return $subdistricts;
    }

    /**
     * @param  $cityId
     * @return string
     */
    public function getCityById($cityId)
    {
        if (empty($cityId)) {
            return '';
        }

        $city = $this->cityFactory->create()->load($cityId);

        return $city->getName();
    }

    /**
     * @param  $subId
     * @return string
     */
    public function getSubdistrictById($subId)
    {
        if (empty($subId)) {
            return '';
        }

        $subdistrict = $this->subdistrictFactory->create()->load($subId);

        return $subdistrict->getName();
    }

    /**
     * @param  $regionId
     * @return string
     */
    public function getRegionById($regionId)
    {
        if (empty($regionId)) {
            return '';
        }

        $region = $this->regionFactory->create()->load($regionId);

        return $region->getDefaultName();
    }

    /**
     * @return int
     */
    public function getCurrentCityId()
    {
        $address = $this->getCurrentAddress();
        return $address ? $address->getCityId() : 0;
    }

    /**
     * @return null
     */
    protected function getCurrentAddress()
    {
        $addressId = $this->_request->getParam('id');
        if (!$addressId) {
            return null;
        }
        $address = $this->_customerAddress->create()->load($addressId);
        if (!$address->getId()) {
            return null;
        }
        return $address;
    }

    /**
     * @return int
     */
    public function getCurrentSubdistrictId()
    {
        $address = $this->getCurrentAddress();
        return $address ? $address->getSubdistrictId() : 0;
    }
}
