/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'ko',
    'underscore',
    'matchMedia',
    'mage/utils/objects',
    'Magento_Ui/js/form/form',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/customer/address',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Tigren_CustomAddress/js/model/billing-address/form-popup-state',
    'Tigren_CustomAddress/js/action/edit-billing-address',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/action/set-billing-address',
    'Magento_Ui/js/modal/modal',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'mage/url'
], function (
    $,
    ko,
    _,
    mediaCheck,
    mageUtils,
    Component,
    customer,
    address,
    addressList,
    addressConverter,
    quote,
    formPopUpState,
    editBillingAddress,
    createBillingAddress,
    selectBillingAddress,
    checkoutData,
    checkoutDataResolver,
    customerData,
    setBillingAddressAction,
    modal,
    globalMessageList,
    $t,
    url
) {
    'use strict';

    var lastSelectedBillingAddress = null,
        newAddressOption = {
            /**
             * Get new address label
             * @returns {String}
             */
            getAddressInline: function () {
                return $t('New Address');
            },
            customerAddressId: null
        },
        countryData = customerData.get('directory-data'),
        addressOptions = addressList().filter(function (address) {
            return address.getType() === 'customer-address'; //eslint-disable-line eqeqeq
        });

    addressOptions.push(newAddressOption);

    var popUp = null;

    return Component.extend({
        defaults: {
            template: 'Tigren_CustomAddress/billing',
            billingFormTemplate: 'Magento_Checkout/billing-address/form',
            modules: {
                postcode: '${ $.name }.address-fieldset.postcode',
                subdistrict: '${ $.name }.address-fieldset.subdistrict_id'
            }
        },
        currentBillingAddress: quote.billingAddress,
        addressOptions: addressOptions,
        customerHasAddresses: addressOptions.length > 1,
        visible: ko.observable(!quote.isVirtual()),
        errorValidationMessage: ko.observable(false),
        isCustomerLoggedIn: customer.isLoggedIn,
        isFormPopUpVisible: formPopUpState.isVisible,
        isFormInline: addressList().length === 0,
        isNewAddressAdded: ko.observable(false),

        hasUpdatedAddress: formPopUpState.hasUpdatedAddress,

        /**
         * Init component
         */
        initialize: function () {
            this._super();
            quote.paymentMethod.subscribe(function () {
                checkoutDataResolver.resolveBillingAddress();
            }, this);
        },

        /**
         * @return {exports.initObservable}
         */
        initObservable: function () {
            var self = this,
                hasNewAddress;

            this._super()
                .observe({
                    selectedAddress: null,
                    isAddressFormVisible: true,
                    isAddressSameAsShipping: false,
                    isAddressFormListVisible: false,
                    saveInAddressBook: 1
                });

            quote.billingAddress.subscribe(function (newAddress) {
                if (quote.isVirtual()) {
                    this.isAddressSameAsShipping(false);
                } else {
                    this.isAddressSameAsShipping(
                        newAddress != null && quote.shippingAddress()
                        && newAddress.getCacheKey() === quote.shippingAddress().getCacheKey() //eslint-disable-line eqeqeq
                    );
                }

                if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                    this.saveInAddressBook(newAddress.saveInAddressBook);
                } else {
                    this.saveInAddressBook(1);
                }
            }, this);

            checkoutDataResolver.resolveBillingAddress();

            hasNewAddress = addressList.some(function (address) {
                return address.getType() === 'new-customer-address' || address.getType() === 'new-billing-address'; //eslint-disable-line eqeqeq
            });

            this.isNewAddressAdded(hasNewAddress);

            this.isFormPopUpVisible.subscribe(function (value) {
                if (value) {
                    self.getPopUp().openModal();
                }
            });

            this.hasUpdatedAddress.subscribe(function (address) {
                if (address) {
                    self.getPopUp().openModal();
                }
            });

            return this;
        },

        getPopUp: function () {
            var self = this,
                buttons;

            if (!popUp) {
                buttons = this.popUpForm.options.buttons;
                this.popUpForm.options.buttons = [
                    {
                        text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                        class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                        click: self.saveNewAddress.bind(self)
                    }
                ];

                /** @inheritdoc */
                this.popUpForm.options.closed = function () {
                    self.isFormPopUpVisible(false);
                };

                this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                this.popUpForm.options.keyEventHandlers = {
                    escapeKey: this.onClosePopUp.bind(this)
                };

                /** @inheritdoc */
                this.popUpForm.options.opened = function () {
                    // Store temporary address for revert action in case when user click cancel action
                    var addressData,
                        isNewAddressPopup = false;

                    if (self.hasUpdatedAddress()) {
                        addressData = addressConverter.quoteAddressToFormAddressData(
                            self.hasUpdatedAddress()
                        );
                        checkoutData.setBillingAddressFromData($.extend(true, {}, addressData));
                        self.hasUpdatedAddress(false);
                    } else {
                        isNewAddressPopup = true;
                    }

                    self.temporaryAddress = $.extend(true, {}, checkoutData.getBillingAddressFromData());

                    if (isNewAddressPopup) {
                        self.reset();
                    } else {
                        _.each(self.temporaryAddress, function (value, name) {
                            if (name === 'custom_attributes') {
                                _.each(value, function (customAttributeValue, customAttributeName) {
                                    if (customAttributeName === 'subdistrict_id') {
                                        this.source.set('billingAddress.custom_attributes.' + customAttributeName, customAttributeValue);
                                        if (this.subdistrict()) {
                                            this.subdistrict().value(customAttributeValue);
                                        }
                                    } else {
                                        this.source.set('billingAddress.custom_attributes.' + customAttributeName, customAttributeValue);
                                    }
                                }, this);
                            } else if (name === 'type') {
                                this.source.set('billingAddress.' + name, 'new-billing-address');
                            } else if (name !== 'postcode') {
                                this.source.set('billingAddress.' + name, value);
                            }
                        }, self);
                        if (self.postcode()) {
                            if (!quote.useDropdown()) {
                                self.postcode().initAddressDataWithPostcode(false);
                                setTimeout(function () {
                                    self.postcode().value(self.temporaryAddress.postcode);
                                }, 600);
                            } else {
                                setTimeout(function () {
                                    self.postcode().value(self.temporaryAddress.postcode);
                                }, 600);
                            }
                        }
                    }
                }
            }

            if (popUp) {
                popUp.closeModal();
                popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
            } else {
                popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
            }

            return popUp;
        },

        /**
         * Update address action
         */
        updateAddress: function () {
            var addressData, newBillingAddress;

            if (this.selectedAddress() && this.selectedAddress() !== newAddressOption) { //eslint-disable-line eqeqeq
                selectBillingAddress(this.selectedAddress());
                checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
            } else {
                this.source.set('params.invalid', false);
                this.source.trigger(this.dataScopePrefix + '.data.validate');

                if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                    this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                }

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get(this.dataScopePrefix);

                    if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                        this.saveInAddressBook(1);
                    }
                    addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);
                }
            }
            this.updateAddresses();
        },

        /**
         * Save new billing address
         */
        saveNewAddress: function () {
            var self = this,
                addressFormData,
                customerAddressId,
                newBillingAddress,
                updatedBillingAddress;

            this.source.set('params.invalid', false);
            this.triggerBillingDataValidateEvent();

            if (!this.source.get('params.invalid')) {
                addressFormData = this.source.get('billingAddress');
                customerAddressId = addressFormData.customer_address_id;

                if (customerAddressId) {
                    $.ajax({
                        url: url.build('custom_address/customer/updateAddress'),
                        method: 'post',
                        data: addressFormData,
                        datatype: 'json',
                        beforeSend: function () {
                            $('body').trigger('processStart');
                        },
                        complete: function () {
                            $('body').trigger('processStop');
                        },
                        success: function (response) {
                            if (response.success) {
                                var addressData = $.extend(true, {}, addressFormData),
                                    region,
                                    regionName = addressData.region;

                                if (mageUtils.isObject(addressData.street)) {
                                    addressData.street = self.objectToArray(addressData.street);
                                }

                                addressData.region = {
                                    'region_id': addressData['region_id'],
                                    'region_code': addressData['region_code'],
                                    region: regionName
                                };

                                if (addressData['region_id'] &&
                                    countryData()[addressData['country_id']] &&
                                    countryData()[addressData['country_id']].regions
                                ) {
                                    region = countryData()[addressData['country_id']].regions[addressData['region_id']];

                                    if (region) {
                                        addressData.region['region_id'] = addressData['region_id'];
                                        addressData.region['region_code'] = region.code;
                                        addressData.region.region = region.name;
                                    }
                                } else if (
                                    !addressData['region_id'] &&
                                    countryData()[addressData['country_id']] &&
                                    countryData()[addressData['country_id']].regions
                                ) {
                                    addressData.region['region_code'] = '';
                                    addressData.region.region = '';
                                }
                                delete addressData['region_id'];

                                updatedBillingAddress = editBillingAddress(address(addressData));
                                selectBillingAddress(updatedBillingAddress);
                                checkoutData.setSelectedBillingAddress(updatedBillingAddress.getKey());
                                self.getPopUp().closeModal();
                                self.hasUpdatedAddress(false);
                            }
                        }
                    });
                } else {
                    // if user clicked the checkbox, its value is true or false. Need to convert.
                    addressFormData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

                    // New address must be selected as a shipping address
                    newBillingAddress = createBillingAddress(addressFormData);
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress($.extend(true, {}, addressFormData));
                    this.getPopUp().closeModal();
                    this.isNewAddressAdded(true);
                }
            }
        },

        /**
         * Edit address action
         */
        editAddress: function () {
            lastSelectedBillingAddress = quote.billingAddress();
            quote.billingAddress(null);
        },

        /**
         * Cancel address edit action
         */
        cancelAddressEdit: function () {
            this.restoreBillingAddress();

            if (quote.billingAddress()) {
                // restore 'Same As Shipping' checkbox state
                this.isAddressSameAsShipping(
                    quote.billingAddress() != null &&
                    quote.billingAddress().getCacheKey() === quote.shippingAddress().getCacheKey() && //eslint-disable-line
                    !quote.isVirtual()
                );
            }
        },

        /**
         * Restore billing address
         */
        restoreBillingAddress: function () {
            if (lastSelectedBillingAddress != null) {
                selectBillingAddress(lastSelectedBillingAddress);
            }
        },

        /**
         * Navigator change hash handler.
         *
         * @param {Object} step - navigation step
         */
        navigate: function (step) {
            step && step.isVisible(true);
        },

        /**
         * Revert address and close modal.
         */
        onClosePopUp: function () {
            checkoutData.setBillingAddressFromData($.extend(true, {}, this.temporaryAddress));
            this.getPopUp().closeModal();
        },

        /**
         * Show address form popup
         */
        showFormPopUp: function () {
            this.isFormPopUpVisible(true);
        },

        /**
         * Trigger action to update shipping and billing addresses
         */
        updateAddresses: function () {
            if (window.checkoutConfig.reloadOnBillingAddress ||
                !window.checkoutConfig.displayBillingOnPaymentMethod
            ) {
                setBillingAddressAction(globalMessageList);
            }
        },

        /**
         * Trigger Billing data Validate Event.
         */
        triggerBillingDataValidateEvent: function () {
            this.source.trigger('billingAddress.data.validate');

            if (this.source.get('billingAddress.custom_attributes')) {
                this.source.trigger('billingAddress.custom_attributes.data.validate');
            }
        },

        canUseBillingAddress: ko.computed(function () {
            return !quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
        }),

        /**
         * @param {Object} address
         * @return {*}
         */
        addressOptionsText: function (address) {
            return address.getAddressInline();
        },

        /**
         * @return {Boolean}
         */
        useShippingAddress: function () {
            if (this.isAddressSameAsShipping()) {
                this.isAddressFormListVisible(false);
            } else {
                if (addressOptions.length === 1) {
                } else {
                    this.isAddressFormListVisible(true);
                }
            }
            return true;
        },
        /**
         * @param {Object} address
         */
        onAddressChange: function (address) {
            // do nothing
        },

        /**
         * @param {int} countryId
         * @return {*}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] !== undefined ? countryData()[countryId].name : '';
        },

        /**
         * Get code
         * @param {Object} parent
         * @returns {String}
         */
        getCode: function (parent) {
            return 'shared';
        },

        /**
         * Convert object to array
         * @param {Object} object
         * @returns {Array}
         */
        objectToArray: function (object) {
            var convertedArray = [];

            $.each(object, function (key) {
                return typeof object[key] === 'string' ? convertedArray.push(object[key]) : false;
            });

            return convertedArray.slice(0);
        }
    });
});