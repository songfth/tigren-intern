/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/**
 * @api
 */
define(
    [
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer'
    ], function ($, quote, customer) {
        'use strict';

        return function (shippingAddress) {
            quote.shippingAddress(shippingAddress);
        };
    }
);
