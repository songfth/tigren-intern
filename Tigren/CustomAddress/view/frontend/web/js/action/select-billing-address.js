/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/**
 * @api
 */
define(
    [
    'jquery',
    'Magento_Checkout/js/model/quote'
    ], function ($, quote) {
        'use strict';

        return function (billingAddress) {
            var address = null;

            if (quote.shippingAddress() && billingAddress.getCacheKey() == //eslint-disable-line eqeqeq
                quote.shippingAddress().getCacheKey()
            ) {
                address = $.extend({}, billingAddress);
                address.saveInAddressBook = null;
            } else {
                address = billingAddress;
            }

            quote.billingAddress(address);
        };
    }
);
