/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

/**
 * @api
 */
define(
    [
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'Magento_Checkout/js/model/default-post-code-resolver'
    ], function (_, registry, Select, defaultPostCodeResolver) {
        'use strict';

        return Select.extend(
            {
                defaults: {
                    modules: {
                        subdistrict: '${ $.parentName }.subdistrict_id',
                        subdistrictInput: '${ $.parentName }.subdistrict',
                        city: '${ $.parentName }.city_id',
                        cityInput: '${ $.parentName }.city',
                        postcode: '${ $.parentName }.postcode'
                    },
                    skipValidation: false,
                    imports: {
                        update: '${ $.parentName }.country_id:value'
                    }
                },

                /**
                 * @param {String} value
                 */
                update: function (value) {
                    var country = registry.get(this.parentName + '.' + 'country_id'),
                    options = country.indexedOptions,
                    isRegionRequired,
                    option;

                    if (!value) {
                        return;
                    }
                    option = options[value];

                    if (typeof option === 'undefined') {
                        return;
                    }

                    defaultPostCodeResolver.setUseDefaultPostCode(!option['is_zipcode_optional']);

                    if (this.skipValidation) {
                        this.validation['required-entry'] = false;
                        this.required(false);
                    } else {
                        if (option && !option['is_region_required']) {
                            this.error(false);
                            this.validation = _.omit(this.validation, 'required-entry');
                        } else {
                            this.validation['required-entry'] = true;
                        }

                        if (option && !this.options().length) {
                            registry.get(
                                this.customName, function (input) {
                                    isRegionRequired = !!option['is_region_required'];
                                    input.validation['required-entry'] = isRegionRequired;
                                    input.required(isRegionRequired);
                                }
                            );
                        }

                        this.required(!!option['is_region_required']);

                        if (option && option.value === 'TH' && this.subdistrict() && this.subdistrictInput() && this.city() && this.cityInput() && this.postcode()) {
                            this.cityInput().validation = _.omit(this.cityInput().validation, 'required-entry');
                            this.cityInput().required(false);
                            this.cityInput().visible(false);
                            this.cityInput().reset();

                            this.city().validation = _.extend(this.city().validation, {'required-entry': true});
                            this.city().required(true);
                            this.city().visible(true);
                            this.city().reset();

                            this.subdistrictInput().visible(false);
                            this.subdistrictInput().validation = _.omit(this.subdistrictInput().validation, 'required-entry');
                            this.subdistrictInput().required(false);
                            this.subdistrictInput().reset();

                            this.subdistrict().visible(true);
                            this.subdistrict().validation = _.extend(this.subdistrict().validation, {'required-entry': true});
                            this.subdistrict().required(true);
                            this.subdistrict().reset();

                            this.postcode().reset();
                        } else if (option && option.value !== 'TH' && this.subdistrict() && this.subdistrictInput() && this.city() && this.cityInput() && this.postcode()) {
                            this.cityInput().validation = _.extend(this.cityInput().validation, {'required-entry': true});
                            this.cityInput().required(true);
                            this.cityInput().visible(true);
                            this.cityInput().reset();

                            this.city().validation = _.omit(this.city().validation, 'required-entry');
                            this.city().required(false);
                            this.city().visible(false);
                            this.city().reset();


                            this.subdistrictInput().visible(false);
                            this.subdistrictInput().validation = _.extend(this.subdistrictInput().validation, {'required-entry': true});
                            this.subdistrictInput().required(false);
                            this.subdistrictInput().reset();

                            this.subdistrict().visible(false);
                            this.subdistrict().validation = _.omit(this.subdistrict().validation, 'required-entry');
                            this.subdistrict().required(false);
                            this.subdistrict().reset();

                            this.postcode().reset();
                        }
                    }
                },

                /**
                 * Filters 'initialOptions' property by 'field' and 'value' passed,
                 * calls 'setOptions' passing the result to it
                 *
                 * @param {*} value
                 * @param {String} field
                 */
                filter: function (value, field) {
                    var country = registry.get(this.parentName + '.' + 'country_id'),
                    option;

                    if (country) {
                        option = country.indexedOptions[value];

                        this._super(value, field);

                        if (option && option['is_region_visible'] === false) {
                            // hide select and corresponding text input field if region must not be shown for selected country
                            this.setVisible(false);

                            if (this.customEntry) {// eslint-disable-line max-depth
                                this.toggleInput(false);
                            }
                        }
                    }
                }
            }
        );
    }
);

