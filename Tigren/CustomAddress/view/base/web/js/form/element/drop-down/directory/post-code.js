/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract'
], function (
    $,
    registry,
    Abstract
) {
    'use strict';

    return Abstract.extend({
        defaults: {
            modules: {
                parent: '${ $.parentName }',
                region: '${ $.parentName }.region_id',
                regionInput: '${ $.parentName }.region',
                city: '${ $.parentName }.city_id',
                cityInput: '${ $.parentName }.city',
                subdistrict: '${ $.parentName }.subdistrict_id',
                subdistrictInput: '${ $.parentName }.subdistrict'
            },
            imports: {
                update: '${ $.parentName }.subdistrict_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var subdistrict = registry.get(this.parentName + '.' + 'subdistrict_id'),
                subdistrictInput = this.subdistrictInput(),
                options = subdistrict.indexedOptions,
                option;

            if (!value) {
                return;
            }

            option = options[value];
            if (typeof option === 'undefined') {
                return;
            }

            if (subdistrictInput) {
                subdistrictInput.value(option.label);
            }

            if (typeof option === 'undefined' || !option.zipcode || option.zipcode === '') {
                this.value('');
            } else {
                this.value(option.zipcode);
            }
        }
    });
});