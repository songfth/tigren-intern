/*
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

define([
    'jquery',
    'uiRegistry',
    'Tigren_CustomAddress/js/form/element/drop-down/directory-select'
], function (
    $,
    registry,
    Select
) {
    'use strict';

    return Select.extend({
        defaults: {
            modules: {
                parent: '${ $.parentName }',
                region: '${ $.parentName }.region_id',
                regionInput: '${ $.parentName }.region',
                city: '${ $.parentName }.city_id',
                cityInput: '${ $.parentName }.city',
                subdistrictInput: '${ $.parentName }.subdistrict',
                postcode: '${ $.parentName }.postcode'
            },
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.city_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var city = registry.get(this.parentName + '.' + 'city_id'),
                cityInput = this.cityInput(),
                options = city.indexedOptions,
                option;

            if (!value) {
                return;
            }

            option = options[value];
            if (typeof option === 'undefined') {
                return;
            }

            if (cityInput) {
                cityInput.value(option.label);
            }

            if (this.skipValidation) {
                this.validation['required-entry'] = false;
                this.required(false);
            } else {
                this.validation['required-entry'] = true;

                if (option && !this.options().length) {
                    registry.get(this.customName, function (input) {
                        input.validation['required-entry'] = true;
                        input.required(true);
                    });
                }

                this.required(true);
            }
        },

        /**
         * Filters 'initialOptions' property by 'field' and 'value' passed,
         * calls 'setOptions' passing the result to it
         *
         * @param {*} value
         * @param {String} field
         */
        filter: function (value, field) {
            var city = registry.get(this.parentName + '.' + 'city_id');

            if (city) {
                this._super(value, field);
            }

            this.reset();
        }
    });
});