<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Override\Magento\Quote\Model;

use Exception;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\InputException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteAddressValidator;
use Magento\Quote\Model\ShippingAddressAssignment;
use Psr\Log\LoggerInterface as Logger;
use Tigren\CustomAddress\Model\CityFactory;
use Tigren\CustomAddress\Model\SubdistrictFactory;

/**
 * Class BillingAddressManagement
 *
 * @package Tigren\CustomAddress\Model\Quote
 */
class BillingAddressManagement extends \Magento\Quote\Model\BillingAddressManagement
{
    /**
     * @var CityFactory
     */
    protected $cityFactory;

    /**
     * @var SubdistrictFactory
     */
    protected $subdistrictFactory;

    /**
     * @var ShippingAddressAssignment
     */
    private $shippingAddressAssignment;

    /**
     * Constructs a quote billing address service object.
     *
     * @param CartRepositoryInterface    $quoteRepository    Quote repository.
     * @param QuoteAddressValidator      $addressValidator   Address validator.
     * @param Logger                     $logger             Logger.
     * @param AddressRepositoryInterface $addressRepository  .
     * @param CityFactory                $cityFactory        .
     * @param SubdistrictFactory         $subdistrictFactory
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        QuoteAddressValidator $addressValidator,
        Logger $logger,
        AddressRepositoryInterface $addressRepository,
        CityFactory $cityFactory,
        SubdistrictFactory $subdistrictFactory
    ) {
        parent::__construct($quoteRepository, $addressValidator, $logger, $addressRepository);
        $this->cityFactory = $cityFactory;
        $this->subdistrictFactory = $subdistrictFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function assign($cartId, AddressInterface $address, $useForShipping = false)
    {
        /**
 * @var Quote $quote
*/
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->removeAddress($quote->getBillingAddress()->getId());
        $quote->setBillingAddress($address);

        $shippingAddressBeforeSave = $quote->getShippingAddress();
        $cityIdOld = $shippingAddressBeforeSave->getCityId();
        $subdistrictIdOld = $shippingAddressBeforeSave->getSubdistrictId();

        try {
            $this->getShippingAddressAssignment()->setAddress($quote, $address, $useForShipping);
            $quote->setDataChanges(true);
            $this->quoteRepository->save($quote);

            /**
  * save custom attributes billing address
*/
            $billingAddress = $quote->getBillingAddress();
            $shippingAddress = $quote->getShippingAddress();
            $extensionAttributes = $address->getExtensionAttributes();

            if ($extensionAttributes && $billingAddress->getId()) {
                $cityId = $extensionAttributes->getCityId();
                $subdistrictId = $extensionAttributes->getSubdistrictId();
                if ($cityId && $subdistrictId) {
                    $this->_updateDataAddress($billingAddress, $cityId, $subdistrictId);
                    $billingAddress->save();
                    if ($useForShipping) {
                        $this->_updateDataAddress($shippingAddress, $cityId, $subdistrictId);
                        $shippingAddress->save();
                    }
                }
            }

            // Save custom attributes to shipping address
            if (!$useForShipping && $cityIdOld && $subdistrictIdOld) {
                $this->_updateDataAddress($shippingAddress, $cityIdOld, $subdistrictIdOld);
                $shippingAddress->save();
            }
        } catch (Exception $e) {
            $this->logger->critical($e);
            throw new InputException(__('Unable to save address. Please check input data.'));
        }
        return $quote->getBillingAddress()->getId();
    }

    /**
     * @return     ShippingAddressAssignment
     * @deprecated 100.2.0
     */
    private function getShippingAddressAssignment()
    {
        if (!$this->shippingAddressAssignment) {
            $this->shippingAddressAssignment = ObjectManager::getInstance()
                ->get(ShippingAddressAssignment::class);
        }
        return $this->shippingAddressAssignment;
    }

    /**
     * @param $address
     * @param $cityId
     * @param $subdistrictId
     */
    protected function _updateDataAddress(&$address, $cityId, $subdistrictId)
    {
        $newCity = $this->cityFactory->create()->load($cityId);
        $city = $newCity->getName();
        $newSubdistrict = $this->subdistrictFactory->create()->load($subdistrictId);
        $subdistrict = $newSubdistrict->getName();

        $address->setCityId($cityId);
        $address->setCity($city);
        $address->setSubdistrictId($subdistrictId);
        $address->setSubdistrict($subdistrict);

        if ($extensionAttributes = $address->getExtensionAttributes()) {
            $customAttributes = [
                'city_id' => $cityId,
                'subdistrict' => $subdistrict,
                'subdistrict_id' => $subdistrictId
            ];

            foreach ($customAttributes as $attributeCode => $attributeValue) {
                $address->setCustomAttribute($attributeCode, $attributeValue);
            }
        }
    }
}
