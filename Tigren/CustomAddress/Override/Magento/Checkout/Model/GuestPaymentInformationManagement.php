<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Override\Magento\Checkout\Model;

use Magento\Checkout\Api\PaymentInformationManagementInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Api\GuestBillingAddressManagementInterface;
use Magento\Quote\Api\GuestCartManagementInterface;
use Magento\Quote\Api\GuestPaymentMethodManagementInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Tigren\CustomAddress\Helper\Data;
use Tigren\CustomAddress\Model\CityFactory;
use Tigren\CustomAddress\Model\SubdistrictFactory;

/**
 * Class PaymentInformationManagement
 *
 * @package Tigren\CustomAddress\Override\Magento\Checkout\Model
 */
class GuestPaymentInformationManagement extends \Magento\Checkout\Model\GuestPaymentInformationManagement
{
    /**
     * @var GuestBillingAddressManagementInterface
     */
    protected $billingAddressManagement;

    /**
     * @var GuestPaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var GuestCartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * @var CityFactory
     */
    protected $cityFactory;
    /**
     * @var SubdistrictFactory
     */
    protected $subdistrictFactory;
    /**
     * @var Data
     */
    private $customAddressHelper;

    /**
     * GuestPaymentInformationManagement constructor.
     *
     * @param GuestBillingAddressManagementInterface $billingAddressManagement
     * @param GuestPaymentMethodManagementInterface  $paymentMethodManagement
     * @param GuestCartManagementInterface           $cartManagement
     * @param PaymentInformationManagementInterface  $paymentInformationManagement
     * @param QuoteIdMaskFactory                     $quoteIdMaskFactory
     * @param CartRepositoryInterface                $cartRepository
     * @param ResourceConnection|null                $connectionPull
     * @param Data                                   $customAddressHelper
     * @param CityFactory                            $cityFactory
     * @param SubdistrictFactory                     $subdistrictFactory
     */
    public function __construct(
        GuestBillingAddressManagementInterface $billingAddressManagement,
        GuestPaymentMethodManagementInterface $paymentMethodManagement,
        GuestCartManagementInterface $cartManagement,
        PaymentInformationManagementInterface $paymentInformationManagement,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $cartRepository,
        ResourceConnection $connectionPull = null,
        Data $customAddressHelper,
        CityFactory $cityFactory,
        SubdistrictFactory $subdistrictFactory
    ) {
        parent::__construct(
            $billingAddressManagement,
            $paymentMethodManagement,
            $cartManagement,
            $paymentInformationManagement,
            $quoteIdMaskFactory,
            $cartRepository,
            $connectionPull
        );
        $this->customAddressHelper = $customAddressHelper;
        $this->cityFactory = $cityFactory;
        $this->subdistrictFactory = $subdistrictFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @throws NoSuchEntityException
     * @throws InvalidTransitionException
     */
    public function savePaymentInformation(
        $cartId,
        $email,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        /**
 * @var Quote $quote
*/
        $quote = $this->cartRepository->getActive($quoteIdMask->getQuoteId());

        if ($billingAddress) {
            $billingAddress->setEmail($email);
            $quote->removeAddress($quote->getBillingAddress()->getId());

            $extensionAttributes = $billingAddress->getExtensionAttributes();
            if ($extensionAttributes) {
                $cityId = $extensionAttributes->getCityId();
                $subdistrictId = $extensionAttributes->getSubdistrictid();
                if ($cityId && $subdistrictId) {
                    $this->updateDataAddress($billingAddress, $cityId, $subdistrictId);
                }
            }

            $quote->setBillingAddress($billingAddress);
            $quote->setDataChanges(true);
        } else {
            $quote->getBillingAddress()->setEmail($email);
        }
        $this->limitShippingCarrier($quote);

        $this->paymentMethodManagement->set($cartId, $paymentMethod);
        return true;
    }

    /**
     * @param $address
     * @param $cityId
     * @param $subdistrictId
     */
    protected function updateDataAddress(&$address, $cityId, $subdistrictId)
    {
        $newCity = $this->cityFactory->create()->load($cityId);
        $city = $newCity->getName();
        $newSubdistrict = $this->subdistrictFactory->create()->load($subdistrictId);
        $subdistrict = $newSubdistrict->getName();

        $address->setCityId($cityId);
        $address->setCity($city);
        $address->setSubdistrictId($subdistrictId);
        $address->setSubdistrict($subdistrict);
    }

    /**
     * Limits shipping rates request by carrier from shipping address.
     *
     * @param Quote $quote
     *
     * @return void
     * @see    \Magento\Shipping\Model\Shipping::collectRates
     */
    private function limitShippingCarrier(Quote $quote)
    {
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress && $shippingAddress->getShippingMethod()) {
            $shippingDataArray = explode('_', $shippingAddress->getShippingMethod());
            $shippingCarrier = array_shift($shippingDataArray);
            $shippingAddress->setLimitCarrier($shippingCarrier);
        }
    }
}
