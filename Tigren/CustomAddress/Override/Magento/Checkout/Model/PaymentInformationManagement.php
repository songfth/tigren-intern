<?php
/**
 * @author    Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2019 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace Tigren\CustomAddress\Override\Magento\Checkout\Model;

use Magento\Checkout\Model\PaymentDetailsFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Quote\Api\BillingAddressManagementInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\Quote\Model\Quote;
use Tigren\CustomAddress\Helper\Data;
use Tigren\CustomAddress\Model\CityFactory;
use Tigren\CustomAddress\Model\SubdistrictFactory;

/**
 * Class PaymentInformationManagement
 *
 * @package Tigren\CustomAddress\Override\Magento\Checkout\Model
 */
class PaymentInformationManagement extends \Magento\Checkout\Model\PaymentInformationManagement
{
    /**
     * @var CityFactory
     */
    protected $cityFactory;
    /**
     * @var SubdistrictFactory
     */
    protected $subdistrictFactory;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var Data
     */
    private $customAddressHelper;

    /**
     * @param              BillingAddressManagementInterface $billingAddressManagement
     * @param              PaymentMethodManagementInterface  $paymentMethodManagement
     * @param              CartManagementInterface           $cartManagement
     * @param              PaymentDetailsFactory             $paymentDetailsFactory    ,
     * @param              CartTotalRepositoryInterface      $cartTotalsRepository
     * @param              Data                              $customAddressHelper
     * @param              CityFactory                       $cityFactory
     * @param              SubdistrictFactory                $subdistrictFactory
     * @codeCoverageIgnore
     */
    public function __construct(
        BillingAddressManagementInterface $billingAddressManagement,
        PaymentMethodManagementInterface $paymentMethodManagement,
        CartManagementInterface $cartManagement,
        PaymentDetailsFactory $paymentDetailsFactory,
        CartTotalRepositoryInterface $cartTotalsRepository,
        Data $customAddressHelper,
        CityFactory $cityFactory,
        SubdistrictFactory $subdistrictFactory
    ) {
        parent::__construct(
            $billingAddressManagement,
            $paymentMethodManagement,
            $cartManagement,
            $paymentDetailsFactory,
            $cartTotalsRepository
        );
        $this->customAddressHelper = $customAddressHelper;
        $this->cityFactory = $cityFactory;
        $this->subdistrictFactory = $subdistrictFactory;
    }

    /**
     * @param  $cartId
     * @param  PaymentInterface      $paymentMethod
     * @param  AddressInterface|null $billingAddress
     * @return bool|int
     * @throws NoSuchEntityException
     * @throws InvalidTransitionException
     */
    public function savePaymentInformation(
        $cartId,
        PaymentInterface $paymentMethod,
        AddressInterface $billingAddress = null
    ) {
        if ($billingAddress) {
            /**
 * @var CartRepositoryInterface $quoteRepository
*/
            $quoteRepository = $this->getCartRepository();
            /**
 * @var Quote $quote
*/
            $quote = $quoteRepository->getActive($cartId);
            $quote->removeAddress($quote->getBillingAddress()->getId());

            $extensionAttributes = $billingAddress->getExtensionAttributes();
            if ($extensionAttributes) {
                $cityId = $extensionAttributes->getCityId();
                $subdistrictId = $extensionAttributes->getSubdistrictid();
                if ($cityId && $subdistrictId) {
                    $this->updateDataAddress($billingAddress, $cityId, $subdistrictId);
                }
            }

            $quote->setBillingAddress($billingAddress);
            $quote->setDataChanges(true);
            $shippingAddress = $quote->getShippingAddress();
            if ($shippingAddress && $shippingAddress->getShippingMethod()) {
                $shippingDataArray = explode('_', $shippingAddress->getShippingMethod());
                $shippingCarrier = array_shift($shippingDataArray);
                $shippingAddress->setLimitCarrier($shippingCarrier);
            }
        }

        $this->paymentMethodManagement->set($cartId, $paymentMethod);

        return true;
    }

    /**
     * Get Cart repository
     *
     * @return     CartRepositoryInterface
     * @deprecated 100.2.0
     */
    private function getCartRepository()
    {
        if (!$this->cartRepository) {
            $this->cartRepository = ObjectManager::getInstance()
                ->get(CartRepositoryInterface::class);
        }
        return $this->cartRepository;
    }

    /**
     * @param $address
     * @param $cityId
     * @param $subdistrictId
     */
    protected function updateDataAddress(&$address, $cityId, $subdistrictId)
    {
        $newCity = $this->cityFactory->create()->load($cityId);
        $city = $newCity->getName();
        $newSubdistrict = $this->subdistrictFactory->create()->load($subdistrictId);
        $subdistrict = $newSubdistrict->getName();

        $address->setCityId($cityId);
        $address->setCity($city);
        $address->setSubdistrictId($subdistrictId);
        $address->setSubdistrict($subdistrict);
    }
}
