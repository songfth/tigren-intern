<?php
/**
 *
 * @package package Lillik\PriceDecimal
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model;

use Magento\Framework\App\CacheInterface;
use Magento\Framework\CurrencyInterface;
use Magento\Framework\Currency as MagentoCurrency;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Currency
 * @package Lillik\PriceDecimal\Model
 */
class Currency extends MagentoCurrency implements CurrencyInterface
{
    use PricePrecisionConfigTrait;

    /**
     * @var ConfigInterface
     */
    public $moduleConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Currency constructor.
     *
     * @param CacheInterface $appCache
     * @param ConfigInterface $moduleConfig
     * @param StoreManagerInterface $storeManager
     * @param null $options
     * @param null $locale
     */
    public function __construct(
        CacheInterface $appCache,
        ConfigInterface $moduleConfig,
        StoreManagerInterface $storeManager,
        $options = null,
        $locale = null
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->_storeManager = $storeManager;
        parent::__construct($appCache, $options, $locale);
    }
}
