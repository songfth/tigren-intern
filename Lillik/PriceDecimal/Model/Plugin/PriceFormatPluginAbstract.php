<?php
/**
 *
 * @package Lillik\PriceDecimal\Model\Plugin
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */


namespace Lillik\PriceDecimal\Model\Plugin;

use Lillik\PriceDecimal\Model\ConfigInterface;
use Lillik\PriceDecimal\Model\PricePrecisionConfigTrait;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PriceFormatPluginAbstract
 * @package Lillik\PriceDecimal\Model\Plugin
 */
abstract class PriceFormatPluginAbstract
{
    use PricePrecisionConfigTrait;

    /**
     * @var ConfigInterface
     */
    protected $moduleConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param ConfigInterface $moduleConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigInterface $moduleConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->moduleConfig  = $moduleConfig;
        $this->_storeManager = $storeManager;
    }
}
